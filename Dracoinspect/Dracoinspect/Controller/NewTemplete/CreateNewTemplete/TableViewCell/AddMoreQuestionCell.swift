//
//  AddMoreQuestionCell.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 17/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class AddMoreQuestionCell: UITableViewCell {

    @IBOutlet weak var btnAddMore: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnAddMore.titleLabel?.font = themeFont(size: 19, fontname: .regular)

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
