import Foundation 
import ObjectMapper 

class ExistingJobModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 
	var data: [ExistingJobList]?

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		data <- map["data"] 
	}
} 

class ExistingJobList: Mappable {

	var createdDatetime: String? 
	var userId: String? 
	var jobName: String? 
	var jobId: String? 
	var lastname: String? 
	var taskList: [TaskListExistingJob]?
	var companyName: String? 
	var updateFormId: String? 
	var firstname: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		createdDatetime <- map["created_datetime"] 
		userId <- map["user_id"] 
		jobName <- map["job_name"] 
		jobId <- map["job_id"] 
		lastname <- map["lastname"] 
		taskList <- map["task_list"] 
		companyName <- map["company_name"] 
		updateFormId <- map["update_form_id"] 
		firstname <- map["firstname"] 
	}
} 

class TaskListExistingJob: Mappable {

	var tblJobTaskSequenceId: String? 
	var jobId: String? 
	var qrCode: String? 
	var tblTemplateFormId: String? 
	var jobKey: String? 
	var formName: String? 
	var sequence: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		tblJobTaskSequenceId <- map["tbl_job_task_sequence_id"] 
		jobId <- map["job_id"] 
		qrCode <- map["qr_code"] 
		tblTemplateFormId <- map["tbl_template_form_id"] 
		jobKey <- map["job_key"] 
		formName <- map["form_name"] 
		sequence <- map["sequence"] 
	}
} 

