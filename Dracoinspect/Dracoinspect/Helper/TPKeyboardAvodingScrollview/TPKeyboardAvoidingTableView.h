//
//  TPKeyboardAvoidingTableView.h
//  TPKeyboardAvoiding
//
//  Created by Haresh on 20/1/2019
//  Copyright 2019 Haresh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+TPKeyboardAvoidingAdditions.h"

@interface TPKeyboardAvoidingTableView : UITableView <UITextFieldDelegate, UITextViewDelegate>
- (BOOL)focusNextTextField;
- (void)scrollToActiveTextField;
@end
