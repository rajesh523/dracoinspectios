//
//  ServicePostFix.swift
//  Draco Support
//
//  Created by Haresh on 26/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import Foundation

let kBasicUsername = "root"
let kBasicPassword = "123"

let basicAdminURL = "http://support.dracofs.com:8080/dracoinspect/"

//MARK: - User URLs
let userURL = "Api_user/"
let apiQuestionsURL = "Api_questions/"
let apiJobURL = "Api_job/"




let registerURL = "register"
let adminListURL = "organization"
let loginURL = "login"
let forgotPasswordURL = "forgot_password"
let logoutURL = "logout"
let questionsAnswerURL = "questions_answer"
let questionsTypeURL = "questionstype"
let templateListURL = "templatelist"
let getExistingTemplateListURL = "getexistingtemplatelist"
let removeTemplateURL = "removetemplate"
let existingTemplateFinishURL = "existing_template_finish"


let taskURL = "add_task"
let createURL = "create_job"
let jobListURL = "job_list"
let existingJobURL = "existing_job"
let updateJobURL = "update_job"


//MARK: - User url

let questionListURL = ""
let jobtaskQrcodeMatchURL = "jobtask_qrcode_match"
let submitAnswerURL = "submit_answer"

//MARK: - Issue DetailsURLs
//let issueDetailURL = "IssueDetail/"
//
//let submitDetailsURL = "submitRequest"
//let requesListURL = "RequestList"
//let finishIssueURL = "finishIssue"
//let startIssueURL = "confirmIssue"
//let QRCodeSetupURL = "QR_setup"
//let needAssistURL = "needAssist"
//let QRCodeDetails = "QR_codeDetail"
//let DeleteDataRequest = "deleteRequest"
//let batchList = "batchlist"
//let inProgress = "Inprogress"
//let technicianList = "technicianlist"


//MARK: - Reports URLs

let reportURL = "reports/"
let historyListURL = "Reportlist"
let addReportURL = "addReport"

//MARK: - Client URL

let clientURL = "client/"
let basicURLForClient = "http://support.dracofs.com:8080/draco-support/client/"
