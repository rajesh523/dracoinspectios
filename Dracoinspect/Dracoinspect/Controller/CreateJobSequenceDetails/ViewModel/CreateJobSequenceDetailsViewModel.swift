//
//  CreateJobSequenceDetailsViewModel.swift
//  Dracoinspect
//
//  Created by Haresh on 28/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class CreateJobSequenceDetailsViewModel
{
    
    //MARK:- Variables
    fileprivate weak var theController:CreateJobSequenceDetailsVc!
    
    init(theController:CreateJobSequenceDetailsVc) {
        self.theController = theController
    }
    
    
//    var arrayTempletList : [TempletList]?
    var selectedTempletList : [AnyObject]?
    
    var arrayTempletList : [AnyObject]?
    
    
    //MARK: - API Calling
    
    func callAddTaskList(isRefreshing:Bool,failed: @escaping (String) -> Void)
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(basicAdminURL)\(apiJobURL)\(taskURL)"
            
            print("URL: \(url)")
            
            var param = [
                        "user_id" : getUserDetail("id"),
                        ]
            
            if theController.selectedJob == ""
            {
                param["tbl_template_form_id"] = ""
            }
            else
            {
                param["tbl_template_form_id"] = theController.selectedJob
            }
            
            print("param :\(param)")
            
            if !isRefreshing
            {
                theController.showLoader()
            }
            
            
            CommonService().PostService(url: url, isLogin: false, param: param) { (response) in
                
                if !isRefreshing
                {
                    self.theController.hideLoader()
                }
                
                (self.theController.view as? CreateJobSequenceDetailsView)?.stopUpperRefresh()
                
                if let json = response.value
                {
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        print("JSON : \(json)")
                        
                        if let data = json.dictionaryObject
                        {
                            if let list = CreateJobSequenceModel(JSON: data)
                            {
                                self.arrayTempletList?.removeAll()
                                self.arrayTempletList = []
                                
                                self.arrayTempletList = list.data
                            }
                            
                            
                            (self.theController.view as? CreateJobSequenceDetailsView)?.tableReload()
                            
                        }
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        
                    }
                    else
                    {
                        failed(json["msg"].stringValue)
                       // makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                     makeToast(strMessage: serverNotResponding)
                }
                
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    
    //MARK: - Selection Set
    
    func selectedCell(index : Int)
    {
        
        if let dict = self.arrayTempletList![index] as? TempletList
        {
            if dict.selected == false
            {
                dict.selected = true
            }
            else
            {
                dict.selected = false
            }
            
            self.arrayTempletList![index] = dict
        }
       
        (self.theController.view as? CreateJobSequenceDetailsView)?.tableReload()
        
    }
    
    
    func selectedTaskList()
    {
        
        self.selectedTempletList = []
        
        guard let array = self.arrayTempletList else {
            print("No array Found")
            return
        }
        
        for item in array
        {
            if let dict = item as? TempletList , dict.selected
            {
                self.selectedTempletList?.append(dict)
            }
            
        }
        
    }
    
    
}
