//
//  UseAnExistingTempleteView.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import SwiftyJSON

class UseAnExistingTempleteView: UIView {

    //MARK:- Variables
    fileprivate weak var theController:UseAnExistingTempleteVC!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblNameTitle: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblEmailTitle: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var lblCompanyNameTitle: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    
    @IBOutlet weak var lblContactNoTitle: UILabel!
    @IBOutlet weak var lblContactNo: UILabel!
    
    @IBOutlet weak var lblTypeTitle: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblNotFound: UILabel!

    
    func setupUI(theDelegate: UseAnExistingTempleteVC) {
        self.theController = theDelegate
        
        lblNameTitle.text = getCommonString(key: "Name_:_key")
        lblNameTitle.font = themeFont(size: 15, fontname: .regular)
        lblName.font = themeFont(size: 15, fontname: .regular)
        
        lblEmailTitle.text = getCommonString(key: "Number_key")
        lblEmailTitle.font = themeFont(size: 15, fontname: .regular)
        lblEmail.font = themeFont(size: 15, fontname: .regular)
        
        lblCompanyNameTitle.text = getCommonString(key: "CompanyName_Key")
        lblCompanyNameTitle.font = themeFont(size: 15, fontname: .regular)
        lblCompanyName.font = themeFont(size: 15, fontname: .regular)
        
        lblContactNoTitle.text = getCommonString(key: "ContactNo_key")
        lblContactNoTitle.font = themeFont(size: 15, fontname: .regular)
        lblContactNo.font = themeFont(size: 15, fontname: .regular)
        
        lblTypeTitle.text = getCommonString(key: "Type_key")
        lblTypeTitle.font = themeFont(size: 15, fontname: .regular)
        lblType.font = themeFont(size: 15, fontname: .regular)
        
        self.setupUserInfo()
        self.theController.mainModelView.templatelistAPICalling()
    }
    
    func setupUserInfo() {
        lblName.text = getUserDetail("firstname") + " " + getUserDetail("lastname")
        lblEmail.text = getUserDetail("email")
        lblCompanyName.text = getUserDetail("company_name")
        lblContactNo.text = getUserDetail("mobile")
        lblType.text = getUserDetail("role") == "1" ? "Admin" : "User"
    }
    
    func reloadData() {
        self.tableView.reloadData()
    }
}
