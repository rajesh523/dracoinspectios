//
//  CreateNewTempleteSecondVC+Tableview.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

extension CreateNewTempleteSecondVC: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.mainViewModel.questionsModel.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let data = self.mainViewModel.questionsModel[section]
        let answer = data.answer
        return 2 + answer!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = self.mainViewModel.questionsModel[indexPath.section]
        let answer = data.answer
        if indexPath.row == 0 {
            let cell: QuestionTitleCell = self.mainView.tableView.dequeueReusableCell(withIdentifier: "QuestionTitleCell") as! QuestionTitleCell
            cell.selectionStyle = .none
            cell.setupData(indexPath: indexPath, data: data)
            return cell
        }
        else if indexPath.row <= answer!.count {
            let cell: QuestionAnswerCell = self.mainView.tableView.dequeueReusableCell(withIdentifier: "QuestionAnswerCell") as! QuestionAnswerCell
            cell.selectionStyle = .none
            cell.setupData(theDelegate: self, indexPath: indexPath, data: data)
            return cell
        }
        else {
            let cell: AddAnswerCell = self.mainView.tableView.dequeueReusableCell(withIdentifier: "AddAnswerCell") as! AddAnswerCell
            cell.selectionStyle = .none
            cell.setupData(theDelegate: self, indexPath: indexPath, data: data)            
            return cell
        }
    }
}
