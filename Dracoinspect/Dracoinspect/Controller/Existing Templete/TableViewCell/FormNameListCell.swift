//
//  FormNameListCell.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 20/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class FormNameListCell: MGSwipeTableCell, MGSwipeTableCellDelegate {
    
    weak var theController:UseAnExistingTempleteVC!
    
    @IBOutlet weak var lblFormName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblFormName.font = themeFont(size: 15, fontname: .regular)
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupData(theDelegate: UseAnExistingTempleteVC, indexPath:IndexPath, data: DataTemplateList) {
        self.theController = theDelegate
        self.delegate = self
        self.tag = indexPath.row
        self.lblFormName.text = data.formName
        
        self.rightButtons = [MGSwipeButton(title: "", icon: UIImage(named:"ic_delete_icon"), backgroundColor: UIColor.appthemeRedColor, callback: { (cell) -> Bool in
            print(cell.tag)
            self.showAlert(formId: data.tblTemplateFormId!, index: self.tag)
            return true
        })]
        self.rightSwipeSettings.transition = .rotate3D
    }
    
    func showAlert(formId: String, index: Int) {
        let alertController = UIAlertController(title: getCommonString(key: "Draco_Inspect_key"), message: getCommonString(key: "Are_you_sure_want_to_delete?_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            self.theController.mainModelView.removeTemplateAPICalling(formId: formId, index: index)
        }
        
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.theController.present(alertController, animated: true, completion: nil)
    }
}
