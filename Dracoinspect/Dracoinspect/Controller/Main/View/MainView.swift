//
//  MainView.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class MainView: UIView {

    //MARK:- Variables
    fileprivate weak var theController:MainVC!
    
    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    func setupUI(theDelegate: MainVC) {
        self.theController = theDelegate
        self.lblName.text = getUserDetail("firstname")
        
        lblWelcome.font = themeFont(size: 21, fontname: .regular)
        lblWelcome.textColor = UIColor.appthemeRedColor
        
        lblName.font = themeFont(size: 21, fontname: .bold)
        lblName.textColor = UIColor.appthemeRedColor
    }

    
    @IBAction func btnCreateNewTaskClicked(_ sender: UIButton) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CreateNewTempleteVC") as! CreateNewTempleteVC
        self.theController.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnModifyExistingTaskClicked(_ sender: Any) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "UseAnExistingTempleteVC") as! UseAnExistingTempleteVC
        self.theController.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnCreateNewJobClicked(_ sender: Any) {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CreateJobSequenceVc") as! CreateJobSequenceVc
        obj.selectedController = .create
        self.theController.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnModifyNewJobClicked(_ sender: Any) {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ModifyJobSequenceVc") as! ModifyJobSequenceVc
        
        self.theController.navigationController?.pushViewController(obj, animated: true)
        
    }
}
