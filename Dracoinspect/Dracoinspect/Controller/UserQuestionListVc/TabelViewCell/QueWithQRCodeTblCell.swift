//
//  QueWithQRCodeTblCell.swift
//  Dracoinspect
//
//  Created by Haresh on 06/06/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class QueWithQRCodeTblCell: UITableViewCell {

    
    //MARK: - Variable
    
    @IBOutlet weak var constantStatusWidth: NSLayoutConstraint!
    @IBOutlet weak var lblnumber: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var vwQrCode: UIView!
    @IBOutlet weak var lblQRCode: UILabel!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblQuestion.font = themeFont(size: 16, fontname: .regular)
        lblQuestion.textColor = UIColor.black
        
        lblStatus.font = themeFont(size: 11, fontname: .regular)
        lblStatus.textColor = UIColor.black
        
        if UIScreen.main.bounds.width <= 320
        {
            self.constantStatusWidth.constant = 85
        }
        else
        {
            self.constantStatusWidth.constant = 105
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupData(data : TaskList,index:Int)
    {
        
        self.lblnumber.text = "\(index+1)."
        self.lblQuestion.text = data.formName
        self.tag = index

        self.vwQrCode.isHidden = data.qrCode == "" ? true : false
        self.lblQRCode.text = data.qrCode == "" ? "" : (getCommonString(key: "Code_No_key") + "" + data.qrCode!)
        
        self.lblQRCode.textColor = UIColor.gray
        
        self.lblStatus.text = data.isAnswerSubmitted == true ? getCommonString(key: "Submitted_key") : getCommonString(key: "Not_submitted_key")
        
        if self.lblStatus.text == getCommonString(key: "Submitted_key")
        {
            self.lblStatus.textColor = UIColor.green
        }
        else
        {
            self.lblStatus.textColor = UIColor.red
        }
        
    }
    
    
}
