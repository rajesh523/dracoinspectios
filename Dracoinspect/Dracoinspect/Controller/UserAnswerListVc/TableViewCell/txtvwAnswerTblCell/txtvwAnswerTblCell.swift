//
//  txtvwAnswerTblCell.swift
//  Dracoinspect
//
//  Created by Haresh on 07/06/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import SwiftyJSON

class txtvwAnswerTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var txtvwAnswer: UITextView!
    
    var indexTxtvw = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        txtvwAnswer.delegate = self
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func settxtvwData(data : QAQuestions,index:Int)
    {
        self.txtvwAnswer.text = data.enterValue
        indexTxtvw = index
        
    }
    
}

extension txtvwAnswerTblCell : UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView)
    {
        if textView == txtvwAnswer {
           // self.lblProblemDescriptionPlaceHolder.isHidden = textView.text == "" ? false : true
            self.txtvwAnswer.text = textView.text
            
        }
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        var json = JSON()
        json["indexPath_textView"].intValue = indexTxtvw
        json["TextValue"].stringValue = textView.text
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SingleTextView"), object: json)
        
    }
    
}
