//
//  AnswerListVc.swift
//  Dracoinspect
//
//  Created by Haresh on 06/06/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import SwiftyJSON

class AnswerListVc: UIViewController {

    //MARK:- Variables
    lazy var theView: AnswerListView = { [unowned self] in
        return self.view as! AnswerListView
        }()
    
    lazy var theViewModel: AnswerListViewModel = {
        return AnswerListViewModel(theController: self)
    }()
    
    var strTempletFormID = ""
    
    var selectedTableTag = 0
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.theView.setupUI()
        self.theViewModel.getExistingJobList()
        
        NotificationCenter.default.addObserver(self, selector: #selector(SingleTextView), name: NSNotification.Name(rawValue: "SingleTextView"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(dropdownValue), name: NSNotification.Name(rawValue: "dropdownValue"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Task_key"))
    }
    
    @IBAction func btnFinishTapped(_ sender: UIButton) {
        
        self.theViewModel.checkValidationForFinishTask()
        
    }

    
    @objc func redirectToScanQRCode()
    {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ScanQRCodeVC") as! ScanQRCodeVC
        obj.selectedController = .submitAnswerUserSide
        obj.mainModelView.delegate = self
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @objc func SingleTextView(_ notification : NSNotification) {
        
        if let object = notification.object as? JSON
        {
            print("object : \(object)")
            
            theViewModel.arrayAnswerList?[0].questions?[object["indexPath_textView"].intValue].enterValue = object["TextValue"].stringValue
            
            (self.view as? AnswerListView)?.tblAnswerList.reloadRows(at: [IndexPath(item: object["indexPath_textView"].intValue, section: 0)], with: .none)
            
        }
    }
    
    @objc func dropdownValue(_ notification : NSNotification) {
        
        if let object = notification.object as? JSON
        {
            print("object : \(object)")
            
            theViewModel.arrayAnswerList?[0].questions?[object["main_index"].intValue].dropdownValueStore = object["value"].stringValue
            
            theViewModel.arrayAnswerList?[0].questions?[object["main_index"].intValue].enterValue = object["dropdown_index"].stringValue
            
             (self.view as? AnswerListView)?.tblAnswerList.reloadRows(at: [IndexPath(item: object["main_index"].intValue, section: 0)], with: .none)
        }
    }
    
    func redirectBackToQuestionList(json: JSON)
    {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadQuestionList"), object: json)
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: UserQuestionListVc.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        
    }
    
}


//MARK: - TableView dataSource and delegate

extension AnswerListVc: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.tag == 100 // Main table
        {
            if theViewModel.arrayAnswerList?.count == 0 || theViewModel.arrayAnswerList == nil
            {
                let lbl = UILabel()
                lbl.text = "Task List is empty."
                lbl.textAlignment = NSTextAlignment.center
                lbl.textColor = UIColor.appthemeRedColor
                lbl.center = tableView.center
                tableView.backgroundView = lbl
                return 0
            }
            
            tableView.backgroundView = nil
            
            return theViewModel.arrayAnswerList?[0].questions?.count ?? 0
        }
        else  // tag = 101 Inner TableCell
        {
            if theViewModel.arrayAnswerList?[0].questions?[tableView.tag].optionList?.count == 0 || theViewModel.arrayAnswerList?[0].questions?[tableView.tag].optionList == nil || theViewModel.arrayAnswerList?[0].questions?[tableView.tag].questionTypeId == "4"
            {
                return 1
            }
            
            tableView.backgroundView = nil
            return theViewModel.arrayAnswerList?[0].questions?[tableView.tag].optionList?.count ?? 0
            
            
        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 100
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QueAndAnswerTblCell") as! QueAndAnswerTblCell
            
            cell.tblInnerAnswer.tag = indexPath.row
            cell.tblInnerAnswer.delegate = self
            cell.tblInnerAnswer.dataSource = self
            
            cell.tblInnerAnswer.reloadData()
            cell.tblInnerAnswer.layoutIfNeeded()
            cell.tblInnerAnswer.layoutSubviews()
            
            cell.heightConstantInnerTbl.constant = cell.tblInnerAnswer.contentSize.height
            
            cell.setupData(data: theViewModel.arrayAnswerList![0].questions![indexPath.row], index: indexPath.row)
            
            return cell
        }
        else
        {
            
            if let data = theViewModel.arrayAnswerList?[0].questions?[tableView.tag]
            {
                
                if data.questionTypeId == "1" //Radio button
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "RadioBtnTblCell") as! RadioBtnTblCell
                    
                    cell.setRadioData(data: data.optionList![indexPath.row], index: indexPath.row)
                    
                    return cell
                }
                else if data.questionTypeId == "2" //Check mark
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "CheckMarkTblCell") as! CheckMarkTblCell
                    
                    cell.setCheckMark(data: data.optionList![indexPath.row], index: indexPath.row)
                    
                    return cell
                }
                else if data.questionTypeId == "3" // TextBox
                {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "txtvwAnswerTblCell") as! txtvwAnswerTblCell
                    
                    cell.settxtvwData(data: data, index: tableView.tag)
                    
                    return cell
                    
                }
                else if data.questionTypeId == "4" // DropDown
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownTblCell") as! DropDownTblCell
                    
                    cell.setDropDownData(data: data, index: tableView.tag)
                    
                    return cell
                }
                else if data.questionTypeId == "5" // QRCode
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "EnterQRCodeTxtfldTblCell") as! EnterQRCodeTxtfldTblCell
                    
                    cell.txtEnterQRCode.isUserInteractionEnabled = false
                    cell.settxtData(data: data, index: indexPath.row)
                    
                    return cell
                }
                
            }
           
        }

        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView.tag != 100
        {
            if let data = theViewModel.arrayAnswerList?[0].questions?[tableView.tag]
            {
                theViewModel.selectionHandle(data: data, index: indexPath.row , tableTag : tableView.tag)
            }
            
        }
        
    }
    
}

extension AnswerListVc: ScanQRCodeDelegate
{
    func ScanQRCode(code: String) {
        print(code)
        
      theViewModel.arrayAnswerList?[0].questions?[selectedTableTag].enterValue = code
      theView.tableReload()
        
    }
    
}
