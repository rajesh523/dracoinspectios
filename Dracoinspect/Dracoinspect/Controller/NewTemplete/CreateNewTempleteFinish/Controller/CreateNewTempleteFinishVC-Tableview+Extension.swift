//
//  CreateNewTempleteFinishVC-.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

extension CreateNewTempleteFinishVC: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.mainViewModel.questionsModel.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        let data = self.mainViewModel.questionsModel[section - 1]
        let answer = data.answer
        return 2 + (data.type != 4 ? answer!.count : 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell: HeaderCell = self.mainView.tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell
            cell.selectionStyle = .none
            cell.setupData(theDelegate: self)            
            return cell
        }
        else {
            let data = self.mainViewModel.questionsModel[indexPath.section - 1]
            let answer = data.answer
            let answerCount = data.type != 4 ? answer!.count : 1
            if indexPath.row == 0 {
                let cell: QuestionTitleCell = self.mainView.tableView.dequeueReusableCell(withIdentifier: "QuestionTitleCell") as! QuestionTitleCell
                cell.selectionStyle = .none
                cell.setupDataFinish(indexPath: indexPath, data: data)
                return cell
            }
            else if indexPath.row <= answerCount {
                if data.type != 4 {
                    let cell: AnswerListCell = self.mainView.tableView.dequeueReusableCell(withIdentifier: "AnswerListCell") as! AnswerListCell
                    cell.setupData(indexPath: indexPath, data: data)
                    return cell
                }
                else {
                    let cell: AnswerListDropDownCell = self.mainView.tableView.dequeueReusableCell(withIdentifier: "AnswerListDropDownCell") as! AnswerListDropDownCell
                    cell.selectionStyle = .none
                    cell.setupData(indexPath: indexPath, data: data)
                    return cell
                }
            }
            else {
                let cell: FooterCell = self.mainView.tableView.dequeueReusableCell(withIdentifier: "FooterCell") as! FooterCell
                cell.selectionStyle = .none
                return cell
            }
        }
    }
}
