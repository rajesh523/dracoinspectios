//
//  DropDownTblCell.swift
//  Dracoinspect
//
//  Created by Haresh on 07/06/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON

class DropDownTblCell: UITableViewCell {

    @IBOutlet weak var txtSelectOptions: CustomTextField!
    
    var selectDD = DropDown()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        txtSelectOptions.delegate = self
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    func setDropDownData(data : QAQuestions, index : Int)
    {
        
        txtSelectOptions.text = data.dropdownValueStore
        
        let arrayDropDown = data.optionList
        
        configTableViewCellDD(dropdown: selectDD, sender: txtSelectOptions)
        
        var arrayStr = [String]()
        
        for i in 0..<arrayDropDown!.count
        {
            arrayStr.append(arrayDropDown?[i].options ?? "")
        }
        
        self.selectDD.dataSource = arrayStr
        self.selectDD.selectionAction = { (indexOfDropDown,item) in
            print("Item:\(item)")
            print("index : \(indexOfDropDown)")
            
            print("self.tag : \(index)")
            
            var json = JSON()
            json["dropdown_index"].intValue = indexOfDropDown + 1
            json["main_index"].intValue = index
            json["value"].stringValue = item
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dropdownValue"), object: json)
            
            self.txtSelectOptions.text = item
            
        }
        
    }
    
    
}

extension DropDownTblCell : UITextFieldDelegate
{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       
        if textField == txtSelectOptions
        {
            self.selectDD.show()
            return false
        }
        
        return true
    }

}
