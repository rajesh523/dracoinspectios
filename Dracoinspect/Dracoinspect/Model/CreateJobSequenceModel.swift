import Foundation 
import ObjectMapper 

class CreateJobSequenceModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 
	var data: [TempletList]?

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		data <- map["data"] 
	}
} 

class TempletList: Mappable { 

	var tblTemplateFormId: String? 
	var companyId: String? 
	var companyName: String? 
	var userId: String? 
	var formName: String?
    var selected: Bool = false

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		tblTemplateFormId <- map["tbl_template_form_id"] 
		companyId <- map["company_id"] 
		companyName <- map["company_name"] 
		userId <- map["user_id"] 
		formName <- map["form_name"]
        
	}
} 

