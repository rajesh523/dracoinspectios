//
//  CreateNewTempleteView.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class CreateNewTempleteView: UIView, ScanQRCodeDelegate {
    
    //MARK:- Variables
    fileprivate weak var theController: CreateNewTempleteVC?

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnNext: CustomButton!
    
    func setupUI(theDelegate: CreateNewTempleteVC) {
        self.theController = theDelegate
        theController?.mainViewModel.questionsModel.removeAll()
        btnNext.setThemeButtonUI()
        btnNext.setTitle(getCommonString(key: "Next_key"), for: .normal)
        
        if !(theController?.mainViewModel.isEdit)! {
            if theController?.loadArray().count == 0 {
                if theController?.mainViewModel.questionCount == 1 {
                    let array : NSMutableArray = NSMutableArray()
                    let dictAnswer: NSDictionary = ["answer": "", "selected": false]
                    array.add(dictAnswer)
                    let dict: NSDictionary = ["question": "", "type": 0, "questionId": "new", "answer": array]
                    theController?.mainViewModel.questionsModel.append(QuestionModel.setModel(dict: dict))
                }
            }
            else {
                theController?.mainViewModel.questionsModel = (theController?.loadArray())!
                theController?.mainViewModel.questionCount = (theController?.mainViewModel.questionsModel.count)!
                self.tableView.reloadData()
            }
            theController?.mainViewModel.txtFormName = (theController?.loadFormName())!
        }
    }
    
    @IBAction func btnAddQuestionClicked(_ sender: Any) {
        var isQuestionAdded:Bool = true
        for data in (theController?.mainViewModel.questionsModel)! {
            let question = data.question
            if question == "" {
                isQuestionAdded = false
            }
        }
        
        if !isQuestionAdded {
            makeToast(strMessage: getCommonString(key:"Please_fill_question_name"))
            return
        }
        let array : NSMutableArray = NSMutableArray()
        let dictAnswer: NSDictionary = ["answer": "", "selected": false]
        array.add(dictAnswer)
        let dict: NSDictionary = ["question": "", "type": 0, "questionId": "new", "answer": array]
        theController?.mainViewModel.questionsModel.append(QuestionModel.setModel(dict: dict))
        
        theController?.mainViewModel.questionCount += 1
        self.tableView.reloadData()
    }
    
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: getCommonString(key: "Draco_Inspect_key"), message: getCommonString(key: "Are_you_sure_want_to_delete?_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            self.theController?.mainViewModel.questionsModel.remove(at: sender.tag)
            
            self.theController?.mainViewModel.questionCount = (self.theController?.mainViewModel.questionCount)! - 1
            self.tableView.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.theController?.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        var isQuestionAdded:Bool = true
        
        if (theController?.mainViewModel.questionsModel)!.count == 0 {
            return
        }
        for data in (theController?.mainViewModel.questionsModel)! {
            let question = data.question
            if question == "" {
                isQuestionAdded = false
            }
        }
        
        if theController?.mainViewModel.txtFormName == "" {
            makeToast(strMessage: getCommonString(key:"Please_enter_task_name"))
            return
        }
        
        if !isQuestionAdded {
            makeToast(strMessage: getCommonString(key:"Please_fill_question_name"))
            return
        }
        
        let obj: CreateNewTempleteSecondVC = theController?.storyboard?.instantiateViewController(withIdentifier: "CreateNewTempleteSecondVC") as! CreateNewTempleteSecondVC
        for (index, data) in (theController?.mainViewModel.questionsModel.enumerated())! {
            if data.type == 3 || data.type == 5 {
                theController?.mainViewModel.questionsModel[index].answer?.removeAll()
            }
            else {
                if data.answer?.count == 0 {
                    theController?.mainViewModel.questionsModel[index].answer?.append(AnswerModel.init(answer: "", selected: false))
                }
            }
        }
        
        obj.mainViewModel.questionsModel = (theController?.mainViewModel.questionsModel)!
        
        if !(self.theController?.mainViewModel.isEdit)! {
            let data = theController?.toNSMutableArray(model: (theController?.mainViewModel.questionsModel)!)
            theController?.saveArray(data: data!)
            theController?.saveFormName(str: (theController?.mainViewModel.txtFormName)!)
            
        }
        else {
            obj.mainViewModel.isEdit = true
            obj.mainViewModel.existingQuestionsModel = (self.theController?.mainViewModel.existingQuestionsModel)!
            obj.mainViewModel.formId = (self.theController?.mainViewModel.formId)!
            obj.mainViewModel.formName = (self.theController?.mainViewModel.txtFormName)!
            obj.mainViewModel.formQRCode = (self.theController?.mainViewModel.formQRCode)!
        }
        theController?.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnScanQRCodeClicked(_ sender: Any) {
        let obj: ScanQRCodeVC = theController?.storyboard?.instantiateViewController(withIdentifier: "ScanQRCodeVC") as! ScanQRCodeVC
        obj.selectedController = .fromNewTemplet
        obj.mainModelView.delegate = self
        theController?.navigationController?.pushViewController(obj, animated: false)
    }
    
    func ScanQRCode(code: String) {
        print(code)
        if !(self.theController?.mainViewModel.isEdit)! {
            self.theController?.saveQRCode(str: code)
        }
        else {
            self.theController?.mainViewModel.formQRCode = code
        }
        self.theController?.mainView.tableView.reloadData()
    }
    
    func reloadData() {
        self.tableView.reloadData()
    }
    
}
