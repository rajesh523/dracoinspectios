//
//  ForgotPasswordViewModel.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:ForgotPasswordVC!
    
    init(theController:ForgotPasswordVC) {
        self.theController = theController
    }
    
    func forgotPasswordAPIcalling() {
        self.theController.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(userURL)\(forgotPasswordURL)"
            
            print("URL: \(url)")
            
            let param = ["lang" : GlobalVariables.strLang,
                         "email" : self.theController.forgotPasswordView.txtEmail.text ?? ""
            ]
            
            print("param :\(param)")
            
            self.theController.showLoader()
            
            CommonService().PostService(url: url,isLogin:true, param: param) { (respones) in
                
                self.theController.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        self.theController.dismiss(animated: false, completion: {
                            makeToast(strMessage: json["msg"].stringValue)
                        })
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        // self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
}
