//
//  AnswerListViewModel.swift
//  Dracoinspect
//
//  Created by Haresh on 06/06/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import Foundation
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON

class AnswerListViewModel
{
    //MARK:- Variables
    fileprivate weak var theController:AnswerListVc!
    
    init(theController:AnswerListVc) {
        self.theController = theController
    }
    
    
    var arrayAnswerList : [QAFormDetail]?
    
    var arrayFinalTaskList = [JSON]()
    
    
    func getExistingJobList()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(basicAdminURL)\(apiQuestionsURL)\(getExistingTemplateListURL)"
            
            print("URL: \(url)")
            
            var param = [
                "user_id" : getUserDetail("id"),
                "tbl_template_form_id" : theController.strTempletFormID
                ]
            
            print("param :\(param)")
            
            theController.showLoader()
            
            CommonService().PostService(url: url, isLogin: false, param: param) { (response) in
                
                self.theController.hideLoader()
                
                (self.theController.view as? ModifyJobSequenceView)?.stopUpperRefresh()
                
                if let json = response.value
                {
                     print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        
                        if let data = json.dictionaryObject
                        {
                            if let list = UserQuestionAnswerModel(JSON: data)
                            {
                                self.arrayAnswerList?.removeAll()
                                self.arrayAnswerList = []
                                
                                self.arrayAnswerList = list.data?.formDetail
                                
                                (self.theController.view as? AnswerListView)?.lblMainTaskName.text = list.data?.formDetail?[0].formName
                                
                                for i in 0..<list.data!.formDetail![0].questions!.count
                                {
                                    let optionCountValue = list.data?.formDetail?[0].questions?[i].optionList?.count
                                    
                                    //list.data?.formDetail?[0].questions?[i].optionCount = list.data!.formDetail?[0].questions?[i].optionList?.count

                                }
                                
                            }
                            
                            (self.theController.view as? AnswerListView)?.tableReload()
                            
                        }
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
                
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    
    
    func selectionHandle(data : QAQuestions , index : Int , tableTag : Int)
    {
        
        if data.questionTypeId == "1" //Radio button
        {
            
            for i in 0..<data.optionList!.count
            {
                 data.optionList?[i].isSelected = "0"
            }
            
            data.optionList?[index].isSelected = data.optionList?[index].isSelected == "1" ? "0" : "1"
            
        }
        else if data.questionTypeId == "2" //Check mark
        {
            data.optionList?[index].isSelected = data.optionList?[index].isSelected == "1" ? "0" : "1"
        }
        else if data.questionTypeId == "3" // TextBox
        {
            
        }
        else if data.questionTypeId == "4" // DropDown
        {
            
        }
        else if data.questionTypeId == "5" // QRCode
        {
            theController.selectedTableTag = tableTag
            theController.redirectToScanQRCode()
        }
        
        (self.theController.view as? AnswerListView)?.tblAnswerList.reloadRows(at: [IndexPath(item: tableTag, section: 0)], with: .none)
        
    }
    
    
    func checkValidationForFinishTask()
    {
        //Question Type id 1,2 and 4 compulsory
        
        self.arrayFinalTaskList = []
        
        var dict = JSON()
        var strArrayAnswer = [String]()
        
        if let data = arrayAnswerList?[0]
        {
            for i in 0..<data.questions!.count
            {
                if data.questions?[i].questionTypeId == "1"
                {
                    
                    if data.questions![i].optionList!.contains(where: { (objOptions) -> Bool in
                        
                        if let index = data.questions![i].optionList!.index(where: { $0.isSelected ==  "1"})
                        {
                            print("index : \(index)")
                            dict["questions_id"].stringValue = data.questions?[i].questionsId ?? "0"
                            dict["question_type_id"].stringValue = data.questions?[i].questionTypeId ?? "0"
                            dict["answer"].stringValue = "\(index + 1)"
                            
                            return true
                            
                        }
                        
                        return false
                    })
                    {
                        print("any one selected")
                        print("Dict : \(dict)")
                        self.arrayFinalTaskList.append(dict)
                        
                    }
                    else
                    {
                        makeToast(strMessage: "Please select any option from single selection")
                        return
                    }
                }
                else if data.questions?[i].questionTypeId == "2"
                {
                    
                    if data.questions![i].optionList!.contains(where: { (objOptions) -> Bool in
                        
                        if objOptions.isSelected == "1"
                        {
                            return true
                        }
                        return false
                        
                    })
                    {
                        print("At Least one selected")
                        
                        if let optionArray = data.questions![i].optionList
                        {
                            
                            for j in 0..<optionArray.count
                            {
                                if optionArray[j].isSelected == "1"
                                {
                                    strArrayAnswer.append("\(j+1)")
                                }
                            }
                            
                            dict["questions_id"].stringValue = data.questions?[i].questionsId ?? "0"
                            dict["question_type_id"].stringValue = data.questions?[i].questionTypeId ?? "0"
                            dict["answer"].stringValue = strArrayAnswer.joined(separator: ",")
                            
                            self.arrayFinalTaskList.append(dict)
                            
                        }
                        
                    }
                    else
                    {
                        makeToast(strMessage: "Please select any option from multiple selection")
                        return
                    }
                }
                else if data.questions?[i].questionTypeId == "3"
                {
                    dict["questions_id"].stringValue = data.questions?[i].questionsId ?? "0"
                    dict["question_type_id"].stringValue = data.questions?[i].questionTypeId ?? "0"
                    dict["answer"].stringValue = data.questions?[i].enterValue ?? ""
                    
                    self.arrayFinalTaskList.append(dict)
                    
                }
                else if data.questions?[i].questionTypeId == "4"
                {
                    if data.questions?[i].enterValue == "" || data.questions?[i].enterValue == nil
                    {
                        makeToast(strMessage: "Please select any option from dropdown selection")
                        return
                    }
                    else
                    {
                        dict["questions_id"].stringValue = data.questions?[i].questionsId ?? "0"
                        dict["question_type_id"].stringValue = data.questions?[i].questionTypeId ?? "0"
                        dict["answer"].stringValue = data.questions?[i].enterValue ?? ""
                        
                        self.arrayFinalTaskList.append(dict)
                    }
                    
                }
                else if data.questions?[i].questionTypeId == "5"
                {
                    dict["questions_id"].stringValue = data.questions?[i].questionsId ?? "0"
                    dict["question_type_id"].stringValue = data.questions?[i].questionTypeId ?? "0"
                    dict["answer"].stringValue = data.questions?[i].enterValue ?? ""
                    
                    self.arrayFinalTaskList.append(dict)
                    
                }
                
            }
        }
        
        var dictFinal = JSON()
        dictFinal["tbl_template_form_id"].stringValue = theController.strTempletFormID
        dictFinal["lattitude"].stringValue = "\(userCurrentLocation?.coordinate.latitude ?? 0.0)"
        dictFinal["longitude"].stringValue = "\(userCurrentLocation?.coordinate.longitude ?? 0.0)"
        dictFinal["answer"].arrayObject = self.arrayFinalTaskList
        
        print("dictFinal:\(dictFinal)")
        
        self.theController.redirectBackToQuestionList(json : dictFinal)
        
    }
    
}
