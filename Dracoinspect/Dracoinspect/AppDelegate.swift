//
//  AppDelegate.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 17/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Alamofire
import CoreLocation
import Firebase
import FirebaseMessaging
import UserNotifications
import SwiftyJSON
import AlamofireSwiftyJSON


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var locationManager = CLLocationManager()
    var lattitude  = Double()
    var longitude = Double()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        
        FirebaseApp.configure()
        
        Messaging.messaging().delegate = self
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.removeAllDeliveredNotifications()
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                print("granted:==\(granted)")
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
                
                if granted {
                    
                    print("granted:==\(granted)")
                    
                }
                else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "grantedFalse"), object: nil)
                }
                
            }
        }
        else
        {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }

        
        if getUserDetail("id") != "" {
            if getUserDetail("role") as? String == "1" // Admin
            {
                GlobalVariables.isUserLogin = false
                
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "MainVC") as! MainVC
                let rearNavigation = UINavigationController(rootViewController: obj)
                appdelgate.window?.rootViewController = rearNavigation
                templatelistAPICalling()
            }
            else
            {
                GlobalVariables.isUserLogin = true
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ModifyJobSequenceVc") as! ModifyJobSequenceVc
                let rearNavigation = UINavigationController(rootViewController: obj)
                appdelgate.window?.rootViewController = rearNavigation
                
            }
        }
        
        setUpQuickLocationUpdate()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        switch CLLocationManager.authorizationStatus()
        {
            case .notDetermined, .restricted, .denied:
            
                if (Defaults.object(forKey:"userDetails") != nil)
                {
                    self.openSetting()
                }
            
            
            case .authorizedAlways, .authorizedWhenInUse:
                break
            
        }
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    
    func templatelistAPICalling() {
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(apiQuestionsURL)\(questionsTypeURL)"
            
            print("URL: \(url)")
            
            let param = ["" : ""]
            
            print("param :\(param)")
            
            self.window?.rootViewController?.showLoader()
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                self.window?.rootViewController?.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        
                        DropDownQuestion = []
                        DropDownQuestionTypeId = []
                        
                        for question in json["data"].arrayValue {
                            DropDownQuestion.append(question["name"].string!)
                            DropDownQuestionTypeId.append(Int(question["question_type_id"].string!)!)
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {

                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Dracoinspect")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}


//MARK:- Location Delegate

extension AppDelegate:CLLocationManagerDelegate
{
    func setUpQuickLocationUpdate()
    {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestLocation()
        self.locationManager.requestWhenInUseAuthorization()
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.distanceFilter = 1
        self.locationManager.startUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
        if let latestLocation = locations.first
        {
            
            print("latestLocation:=\(latestLocation.coordinate.latitude), *=\(latestLocation.coordinate.longitude)")
            
            if latestLocation == nil
            {
                setUpQuickLocationUpdate()
                return
            }
            
            userCurrentLocation = latestLocation
            
            if lattitude != latestLocation.coordinate.latitude && longitude != latestLocation.coordinate.longitude
            {
                userCurrentLocation = latestLocation
                lattitude = latestLocation.coordinate.latitude
                longitude = latestLocation.coordinate.longitude
                
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            //    manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            // setupUserCurrentLocation()
            
            //  manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            //  setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            if #available(iOS 10.0, *) {
                openSetting()
                
            } else {
                // Fallback on earlier versions
            }
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            if #available(iOS 10.0, *) {
                openSetting()
                
            } else {
                // Fallback on earlier versions
            }
            break
            //   default:
            // break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error :- \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        locationManager.stopUpdatingLocation()
    }
    
    
    //MARK:- open Setting
    
    func openSetting()
    {
        let alertController = UIAlertController (title: getCommonString(key: "Draco_Inspect_key"), message: getCommonString(key: "Go_to_Setting_key"), preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: getCommonString(key: "Setting_key"), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(settingsAction)
        //     let cancelAction = UIAlertAction(title: mapping.string(forKey: "Cancel_key"), style: .default, handler: nil)
        //    alertController.addAction(cancelAction)
        
        window?.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
}


//MARK: - Usernotification delegate

extension AppDelegate
{
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print("User Info = ",notification.request.content.userInfo)
        
        let dict = JSON(notification.request.content.userInfo)
        
        print("Dict : \(dict)")
        
        if getUserDetail("id") == ""
        {
            return
        }
        
        completionHandler([.alert,.badge,.sound])
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        
        print("Device Token = ", token)
        
        UserDefaults.standard.set(token, forKey: "device_token")
        UserDefaults.standard.synchronize()
        
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("ERROR : \(error)")
    }
    
    // While Banner Tap and App in Background mode..
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Tap on Banner Push for ios 10 version")
        
        let jsonDict = response.notification.request.content.userInfo
        
        print("JSON DICt - ",JSON(jsonDict))
        
        let dict = JSON(jsonDict)
        
        if getUserDetail("id") == ""
        {
            return
        }
        
    }
    
    //MARK: - Firebase Messeging delegate methods
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String)
    {
        print("Firebase registration token: \(fcmToken)")
        connectToFcm()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                //  self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
            }
        }
        
        Messaging.messaging().shouldEstablishDirectChannel = false
        
        /*
         guard InstanceID.instanceID().token() != nil else {
         return
         }
         
         // Disconnect previous FCM connection if it exists.
         
         Messaging.messaging().connect() { (error) in
         if error != nil {
         print("Unable to connect with FCM. \(error?.localizedDescription ?? "")")
         } else {
         print("Connected to FCM.")
         }
         }
         */
    }
    
}
