//
//  CreateJobSequenceView.swift
//  Dracoinspect
//
//  Created by Haresh on 28/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import HPReorderTableView

class CreateJobSequenceView: UIView {

    //MARK: - Outlet
    
    @IBOutlet weak var txtEnterForm: UITextField!
    @IBOutlet weak var tblJob: HPReorderAndSwipeToDeleteTableView!
    @IBOutlet weak var btnAddTaskForthisJob: UIButton!
    @IBOutlet weak var btnFinish: CustomButton!
    @IBOutlet weak var heightConstantOfBtnNext: NSLayoutConstraint!
    

    func setupUI()
    {
        btnAddTaskForthisJob.layer.cornerRadius = 8
        btnAddTaskForthisJob.layer.masksToBounds = true
        btnAddTaskForthisJob.backgroundColor = UIColor.appthemeRedColor
        
        tblJob.register(UINib(nibName: "arrangeAndDeleteTblCell", bundle: nil), forCellReuseIdentifier: "arrangeAndDeleteTblCell")
        tblJob.tableFooterView = UIView()
        tblJob.isEditing = false
        
        tblJob.allowsSelectionDuringEditing = false
        
    }
    
    func reloadTable()
    {
        self.tblJob.reloadData()
    }
    
    func btnFinish(isHidden : Bool)
    {
        if isHidden
        {
            self.btnFinish.isHidden = true
            self.heightConstantOfBtnNext.constant = 0
        }
        else
        {
            self.btnFinish.isHidden = false
            self.heightConstantOfBtnNext.constant = 50
        }
    }
    
    func checkValidation(theViewModel:CreateJobSequenceViewModel) -> Bool
    {
        
        if (txtEnterForm.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: getCommonString(key:"Please_enter_the_form_key"))
            return false
        }
        
        theViewModel.strJobName = txtEnterForm.text!
        
        return true
    }
    
   
    
}
