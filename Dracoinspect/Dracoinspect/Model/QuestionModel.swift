//
//  QuestionModel.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 17/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class QuestionModel: NSObject {
    var question: String?
    var type: Int?
    var questionId: String?
    var answer: [AnswerModel]?

    init(question:String?, type: Int?, questionId: String?, answer: [AnswerModel]?){
        self.question = question
        self.type = type
        self.questionId = questionId
        self.answer = answer
    }
    
    public class func setModel(dict: NSDictionary) -> QuestionModel {
        let question = dict.value(forKey: "question") as! String
        let type = dict.value(forKey: "type") as! Int
        let questionId = dict.value(forKey: "questionId") as! String
//        let answer = dict.value(forKey: "answer") as! NSMutableArray
        let answer = (dict.value(forKey: "answer") as! NSArray).mutableCopy() as! NSMutableArray

        var answerModel: [AnswerModel] = [AnswerModel]()
        for data in answer {
            let dictionary = data as! NSDictionary
            let answer = dictionary.value(forKey: "answer") as! String
            let selected = dictionary.value(forKey: "selected") as! Bool
            answerModel.append(AnswerModel.init(answer: answer, selected: selected))
        }
        return QuestionModel.init(question: question, type: type, questionId: questionId, answer: answerModel)
    }
}

class AnswerModel: NSObject {
    var answer: String?
    var selected: Bool?
    
    init(answer:String?, selected: Bool?){
        self.answer = answer
        self.selected = selected
    }
}

extension UIViewController {
    
    func toNSMutableArray(model: [QuestionModel]) -> NSMutableArray {
        let questions: NSMutableArray = NSMutableArray()
        for data in model {
            let ans: NSMutableArray = NSMutableArray()
            for dataAns in data.answer! {
                let dict: NSDictionary = ["answer":dataAns.answer!, "selected": dataAns.selected!]
                ans.add(dict)
            }
            let dict: NSDictionary = ["question":data.question!, "type": data.type!, "questionId":data.questionId!, "answer":ans]
            questions.add(dict)
        }
        return questions
    }
    
    func loadModel(array: NSMutableArray) -> [QuestionModel] {
        var questions: [QuestionModel] = [QuestionModel]()
        for data in array {
            questions.append(QuestionModel.setModel(dict: data as! NSDictionary))
        }
        return questions
    }
    
    func toModel(array: NSMutableArray) {
        var questions: [QuestionModel] = [QuestionModel]()
        for data in array {
            let dataValue = data as! NSDictionary
            let answerArray = dataValue.value(forKey: "answer") as! NSDictionary

            var ans: [AnswerModel] = [AnswerModel]()
            for dataAns in answerArray {
                let dataAnsValue = dataAns as! NSDictionary
                let answer = dataAnsValue.value(forKey: "answer") as! String
                let selected = dataAnsValue.value(forKey: "selected") as! Bool
                let model: AnswerModel = AnswerModel.init(answer: answer, selected: selected)
                ans.append(model)
            }
            
            let question = dataValue.value(forKey: "question") as! String
            let type = dataValue.value(forKey: "type") as! Int
            let questionId = dataValue.value(forKey: "questionId") as! String
            let model: QuestionModel = QuestionModel.init(question: question, type: type, questionId: questionId, answer: ans)
            questions.append(model)
        }
    }
}
