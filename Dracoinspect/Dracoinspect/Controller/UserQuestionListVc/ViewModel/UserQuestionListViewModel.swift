//
//  UserQuestionListViewModel.swift
//  Dracoinspect
//
//  Created by Haresh on 06/06/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class UserQuestionListViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:UserQuestionListVc!
    
    init(theController:UserQuestionListVc) {
        self.theController = theController
    }
    
    var arrayTaskList : [TaskList]?

    var arraySubmitFinalTask = [JSON]()
    
    //MARK: - API calling
    
    func callFinishTaskAPI()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(basicAdminURL)\(apiJobURL)\(submitAnswerURL)"
            
            print("URL: \(url)")
            
            
            let param : [String : Any] = [
                "user_id" : getUserDetail("id"),
                "job_id" : theController.strJobId,
                "answer" : JSON(self.arraySubmitFinalTask).rawString() ?? ""
                ]
            
            print("param :\(param)")
          
            theController.showLoader()
            
            CommonService().PostService(url: url, isLogin: false, param: param) { (response) in
                
                self.theController.hideLoader()
                
                if let json = response.value
                {
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        print("JSON : \(json)")
                        
                        if let data = json.dictionaryObject
                        {
                          
                            makeToast(strMessage: json["msg"].stringValue)
                            self.theController.navigationController?.popViewController(animated: true)
                        }
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
                
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    
    
}
