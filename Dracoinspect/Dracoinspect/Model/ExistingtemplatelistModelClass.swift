import Foundation 
import ObjectMapper 

class ExistingTemplatelistModelClass: Mappable {

	var data: DataExistingTemplate? 
	var flag: NSNumber? 
	var msg: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		data <- map["data"] 
		flag <- map["flag"] 
		msg <- map["msg"] 
	}
} 

class DataExistingTemplate: Mappable {

	var formDetail: [FormDetail]? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		formDetail <- map["form_detail"] 
	}
} 

class FormDetail: Mappable { 

	var questions: [Questions]? 
	var tblTemplateFormId: String? 
	var updateFormId: String? 
	var qrCode: String? 
	var modifiedDate: String? 
	var createdDate: String? 
	var formName: String? 
	var userId: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		questions <- map["questions"] 
		tblTemplateFormId <- map["tbl_template_form_id"] 
		updateFormId <- map["update_form_id"] 
		qrCode <- map["qr_code"] 
		modifiedDate <- map["modified_date"] 
		createdDate <- map["created_date"] 
		formName <- map["form_name"] 
		userId <- map["user_id"] 
	}
} 

class Questions: Mappable { 

	var questionsId: String? 
	var questionTypeId: String? 
	var optionList: [OptionList]? 
	var questionsName: String? 
	var tblTemplateFormId: String? 
	var status: String? 
	var modifiedDate: String? 
	var createdDate: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		questionsId <- map["questions_id"] 
		questionTypeId <- map["question_type_id"] 
		optionList <- map["option_list"] 
		questionsName <- map["questions_name"] 
		tblTemplateFormId <- map["tbl_template_form_id"] 
		status <- map["status"] 
		modifiedDate <- map["modified_date"] 
		createdDate <- map["created_date"] 
	}
} 

class OptionList: Mappable { 

	var questionsId: String? 
	var tblOptionsId: String? 
	var createdDate: String? 
	var options: String? 
	var trueAnswer: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		questionsId <- map["questions_id"] 
		tblOptionsId <- map["tbl_options_id"] 
		createdDate <- map["created_date"] 
		options <- map["options"] 
		trueAnswer <- map["true_answer"] 
	}
} 

