import Foundation 
import ObjectMapper

class TemplateListModelClass: Mappable { 

	var data: [DataTemplateList]? 
	var msg: String? 
	var flag: NSNumber? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		data <- map["data"] 
		msg <- map["msg"] 
		flag <- map["flag"] 
	}
} 

class DataTemplateList: Mappable {

	var companyName: String? 
	var tblTemplateFormId: String? 
	var userId: String? 
	var formName: String? 
	var isAnswer: String? 
	var companyId: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		companyName <- map["company_name"] 
		tblTemplateFormId <- map["tbl_template_form_id"] 
		userId <- map["user_id"] 
		formName <- map["form_name"] 
		isAnswer <- map["is_answer"] 
		companyId <- map["company_id"] 
	}
} 

