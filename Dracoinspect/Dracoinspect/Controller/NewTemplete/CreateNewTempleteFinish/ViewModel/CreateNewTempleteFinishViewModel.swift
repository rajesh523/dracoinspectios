//
//  CreateNewTempleteFinishViewModel.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CreateNewTempleteFinishViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:CreateNewTempleteFinishVC!
    
    init(theController:CreateNewTempleteFinishVC) {
        self.theController = theController
    }
    
    var questionsModel: [QuestionModel] = [QuestionModel]()
    var existingQuestionsModel :ExistingTemplatelistModelClass?
    var isEdit:Bool = false
    var formId:String = ""
    var formName:String = ""
    var formQRCode:String = ""
    
    func questionsAnswerAPICalling() {
        theController?.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(basicAdminURL)\(apiQuestionsURL)\(questionsAnswerURL)"
            print("URL: \(url)")
            
            let array:NSMutableArray = NSMutableArray()
            for data in self.questionsModel {
                let arrayAnswer:NSMutableArray = NSMutableArray()
                for dataValue in data.answer! {
                    let answer = dataValue.answer
                    let dictAnswer: NSDictionary = ["options":answer!]
                    arrayAnswer.add(dictAnswer)
                }
                let dict: NSDictionary = ["questionname":data.question!, "question_type_id":data.type!, "answer":arrayAnswer]
                array.add(dict)
            }
            
            let param = ["user_id" : getUserDetail("id"),
                         "form_name" : theController.loadFormName(),
                         "qr_code" : theController.loadQRCode(),
                         "questions_answer" :"\(JSON(array))"
                ] as [String : Any]
            
            print("param :\(param)")
            theController?.showLoader()
            CommonService().PostService(url: url, param: param) { (respones) in
                
                self.theController?.hideLoader()
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.theController.removeQuestion()

                        for controller in self.theController.navigationController!.viewControllers as Array {
                            if controller.isKind(of: MainVC.self) {
                                self.theController.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }

                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.theController?.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
    
    func existingTemplateFinishAPICalling() {
        theController?.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(basicAdminURL)\(apiQuestionsURL)\(existingTemplateFinishURL)"
            print("URL: \(url)")
            
            let array:NSMutableArray = NSMutableArray()
            for data in self.questionsModel {
                let arrayAnswer:NSMutableArray = NSMutableArray()
                for dataValue in data.answer! {
                    let answer = dataValue.answer
                    let dictAnswer: NSDictionary = ["options":answer!]
                    arrayAnswer.add(dictAnswer)
                }
                let dict: NSDictionary = ["questions_id":data.questionId!,"questionname":data.question!, "question_type_id":data.type!, "answer":arrayAnswer]
                array.add(dict)
            }
            
            var questionsModelNew: [String] = [String]()
            for data in (existingQuestionsModel?.data?.formDetail)! {
                for dataValue in data.questions! {
                    var questionDeleted:Bool = true
                    for dataModel in questionsModel {
                        if dataValue.questionsId == dataModel.questionId {
                            questionDeleted = false
                        }
                    }
                    if questionDeleted {
                        questionsModelNew.append(dataValue.questionsId!)
                    }
                }
            }
            
            let param = ["user_id" : getUserDetail("id"),
                         "tbl_template_form_id": self.formId,
                         "form_name" : self.formName,
                         "qr_code" : self.formQRCode,
                         "delete_question" : questionsModelNew.joined(separator: ","),
                         "questions_answer" :"\(JSON(array))"
                ] as [String : Any]
            
            print("param :\(param)")
            theController?.showLoader()
            CommonService().PostService(url: url, param: param) { (respones) in
                
                self.theController?.hideLoader()

                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        makeToast(strMessage: json["msg"].stringValue)
                        
                        for controller in self.theController.navigationController!.viewControllers as Array {
                            if controller.isKind(of: MainVC.self) {
                                self.theController.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.theController?.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
}
