//
//  ScanQRCodeView.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import QRCodeReader
import AVFoundation
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON

class ScanQRCodeView: UIView, UITextFieldDelegate {

    //MARK:- Variables
    fileprivate weak var theController:ScanQRCodeVC!
    //MARK: - Outlet    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblDetailsMsg: UILabel!
    
    @IBOutlet weak var txtQRCodeEnter: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var vwScanner: QRCodeReaderView!{
        didSet {
            vwScanner.setupComponents(with: QRCodeReaderViewControllerBuilder {
                $0.reader                 = reader
                $0.showTorchButton        = false
                $0.showSwitchCameraButton = false
                $0.showCancelButton       = false
                $0.showOverlayView        = false
                
                // $0.rectOfInterest         = CGRect(x: 0.0, y: 0.0, width: 0.6, height: 0.6)
            })
        }
    }
    
    lazy var reader: QRCodeReader = QRCodeReader()

    func setupUI(theDelegate: ScanQRCodeVC) {
        self.theController = theDelegate
        //CheckPermission
        self.vwMain.backgroundColor = UIColor.appThemeDarkGrayColor
        
        self.lblDetailsMsg.textColor = UIColor.white
        self.lblDetailsMsg.font = themeFont(size: 16, fontname: .regular)
        self.lblDetailsMsg.text = getCommonString(key: "Please_put_your_code_into_the_rectangle_view_finder_to_scan_key")
        
        txtQRCodeEnter.delegate = self
        txtQRCodeEnter.placeholder = getCommonString(key: "Enter_QR_Code_key")
        txtQRCodeEnter.font = themeFont(size: 16, fontname: .regular)
        txtQRCodeEnter.textColor = UIColor.black
        txtQRCodeEnter.tintColor = UIColor.appthemeRedColor
        txtQRCodeEnter.backgroundColor = UIColor.white
        
        btnSubmit.setThemeButtonUI()
        btnSubmit.layer.cornerRadius = 5.0
        btnSubmit.setTitle(getCommonString(key: "Submit_key"), for: .normal)
        
        theController.addDoneButtonOnKeyboard(textfield: txtQRCodeEnter)
        
        guard theController.mainModelView.checkScanPermissions(), !reader.isRunning else { return }
        reader.startScanning()
        reader.didFindCode = { result in
            print("Completion with result: \(result.value) of type \(result.metadataType)")
            
            print("result:\(result)")
            
            let scanDataString = result.value
            self.txtQRCodeEnter.text = scanDataString
        }
    }
    
    func stopScanning() {
        isScanScreenForIssue = false
        reader.stopScanning()
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
        theController.view.endEditing(true)
        
        if (txtQRCodeEnter.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_QR_code_key"))
        }
        else if (txtQRCodeEnter.text?.isNumeric == false)
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_valid_QR_code_key"))
        }
        else
        {
            theController.mainModelView.redirectToSubmitDetails(qrScanData: txtQRCodeEnter.text!)
//            theController.mainModelView.checkQRRegisterOrNot(qrString: txtQRCodeEnter.text!)
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        
        reader.stopScanning()
        
        theController.dismiss(animated: true) { [weak self] in
            let alert = UIAlertController(
                title: "QRCodeReader",
                message: String (format:"%@ (of type %@)", result.value, result.metadataType),
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            
            self?.theController.present(alert, animated: true, completion: nil)
        }
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        print("Switching capture to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        theController.dismiss(animated: true, completion: nil)
        
    }
}
