//
//  CreateNewTempleteSecondView.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class CreateNewTempleteSecondView: UIView {

    //MARK:- Variables
    fileprivate weak var theController: CreateNewTempleteSecondVC?
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnFinish: CustomButton!
    
    func setupUI(theDelegate: CreateNewTempleteSecondVC) {
        self.theController = theDelegate
        
        btnFinish.setThemeButtonUI()
        btnFinish.setTitle(getCommonString(key: "Finish_key"), for: .normal)
    }
    
    @IBAction func btnFinishClicked(_ sender: Any) {
        var isAnswerAdded:Bool = true
        for data in (self.theController?.mainViewModel.questionsModel)! {
            for dataValue in data.answer! {
                let answer = dataValue.answer
                if answer == "" {
                    isAnswerAdded = false
                }
            }
        }
        
        if !isAnswerAdded {
            makeToast(strMessage: getCommonString(key:"Please_fill_all_fields"))
            return
        }
        let obj: CreateNewTempleteFinishVC = self.theController?.storyboard?.instantiateViewController(withIdentifier: "CreateNewTempleteFinishVC") as! CreateNewTempleteFinishVC
        obj.mainViewModel.questionsModel = (self.theController?.mainViewModel.questionsModel)!
        
        if !(self.theController?.mainViewModel.isEdit)! {
            let data = self.theController?.toNSMutableArray(model: (self.theController?.mainViewModel.questionsModel)!)
            self.theController?.saveArray(data: data!)
        }
        else {
            obj.mainViewModel.isEdit = true
            obj.mainViewModel.existingQuestionsModel = (self.theController?.mainViewModel.existingQuestionsModel)!
            obj.mainViewModel.formId = (self.theController?.mainViewModel.formId)!
            obj.mainViewModel.formName = (self.theController?.mainViewModel.formName)!
            obj.mainViewModel.formQRCode = (self.theController?.mainViewModel.formQRCode)!
        }
        
        self.theController?.navigationController?.pushViewController(obj, animated: true)
    }

    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.theController?.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddMoreClicked(_ sender: UIButton) {
        print(sender.tag)
        let data = self.theController?.mainViewModel.questionsModel[sender.tag]
        var answer = data!.answer
        
        var isAddedAnswer:Bool = true
        for data in answer! {
            let answer = data.answer
            if answer == "" {
                isAddedAnswer = false
            }
        }
        
        if !isAddedAnswer {
            makeToast(strMessage: getCommonString(key:"Please_fill_answer"))
            return
        }
        
        answer?.append(AnswerModel.init(answer: "", selected: false))
        data!.answer = answer
        self.theController?.mainViewModel.questionsModel[sender.tag] = data!
        self.tableView.reloadData()
    }
}
