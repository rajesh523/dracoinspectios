//
//  LoginVC.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import Alamofire

class LoginView: UIView, UITextFieldDelegate {
    
    //MARK:- Variables
    fileprivate weak var theController: LoginVC?
    
    //MARK: - @IBOutlet
    
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    
    @IBOutlet weak var vwEmail: CustomView!
    @IBOutlet weak var vwPassword: CustomView!
    
    @IBOutlet weak var btnForgotPassword: UIButton!
    
    @IBOutlet weak var btnLogin: CustomButton!
    @IBOutlet weak var btnForSignUp: UIButton!
    
    @IBOutlet weak var imgViewCheckMark: UIImageView!
    @IBOutlet weak var lblSavePassword: UILabel!
    @IBOutlet weak var btnSavePassword: UIButton!
    
    func setupUI(theDelegate: LoginVC) {
        self.theController = theDelegate
        imgViewCheckMark.image = UIImage(named: "ic_check_icon_off")
        
        if (Defaults.string(forKey: "imageCheckMark") == "ic_check_icon_on") {
            txtEmail.text = Defaults.string(forKey: "emailStore")
            txtPassword.text = Defaults.string(forKey: "passworldStore")
            let image = Defaults.string(forKey: "imageCheckMark")
            imgViewCheckMark.image = UIImage(named: image ?? "ic_check_icon_off")
        }
        
        self.lblSavePassword.textColor = UIColor.lightGray
        self.lblSavePassword.font = themeFont(size: 14, fontname: .regular)
        
        [txtEmail,txtPassword].forEach { (txt) in
            txt?.delegate = self
            txt?.leftPaddingView = 55
            txt?.setThemeTextFieldUI()
            
        }
        
        [vwEmail,vwPassword].forEach { (vw) in
            vw?.cornerRadius = (vw!.bounds.height)/2
            vw?.borderWidth = 1.0
            vw?.borderColors = UIColor.appthemeRedColor
            vw?.shadowColors = UIColor.lightGray
            vw?.shadowRadius = 2.0
            vw?.shadowOffset = CGSize(width: 1.0, height: 1.0)
            vw?.shadowOpacity = 0.8
        }
        
        txtEmail.placeholder = getCommonString(key: "Email_key")
        txtPassword.placeholder = getCommonString(key: "Password_key")
        
        btnForgotPassword.setTitle(getCommonString(key: "Forgot_Password_?_key"), for: .normal)
        btnForgotPassword.titleLabel?.font = themeFont(size: 14, fontname: .regular)
        btnForgotPassword.setTitleColor(UIColor.lightGray, for: .normal)
        
        btnLogin.setThemeButtonUI()
        btnLogin.setTitle(getCommonString(key: "Login_key"), for: .normal)
        
        btnForSignUp.setTitle(getCommonString(key: "New_user_?_Sign_up_key"), for: .normal)
        btnForSignUp.titleLabel?.font = themeFont(size: 14, fontname: .regular)
        btnForSignUp.setTitleColor(UIColor.lightGray, for: .normal)
        btnForSignUp.titleLabel?.textAlignment = .right
    }   
    
    @IBAction func loginTapped(_ sender:UIButton) {
        if (txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: getCommonString(key:"Please_enter_email_key"))
        }
        else if (txtPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: getCommonString(key:"Please_enter_password_key"))
        }
        else if (txtPassword.text!.count) < 6 {
            makeToast(strMessage: getCommonString(key:"Password_must_be_at_least_6_characters_long_key"))
        }
        else {
            self.theController?.loginModelView.saveEmailPassword()
            self.theController?.loginModelView.loginAPICalling()
        }
    }
    
    @IBAction func btnForgotPasswordTapped(_ sender: UIButton) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        theController?.present(obj, animated: false, completion: nil)
    }
    
    @IBAction func btnSignpTapped(_ sender: UIButton) {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        theController?.navigationController?.pushViewController(obj, animated: true)
    }
    
    
    @IBAction func btnSavePassword(_ sender: Any) {
        if (imgViewCheckMark.image == UIImage(named: "ic_check_icon_off")) {
            imgViewCheckMark.image = UIImage(named: "ic_check_icon_on")
        }
        else {
            imgViewCheckMark.image = UIImage(named: "ic_check_icon_off")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
}
