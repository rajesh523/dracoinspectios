import Foundation 
import ObjectMapper 

class ModifyJobSequenceModel: Mappable { 

	var data: [ModifyList]? 
	var flag: NSNumber? 
	var msg: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		data <- map["data"] 
		flag <- map["flag"] 
		msg <- map["msg"] 
	}
} 

class ModifyList: Mappable {

	var createdDatetime: String? 
	var updateFormId: String? 
	var jobId: String? 
	var jobName: String? 
	var companyName: String? 
	var firstname: String? 
	var userId: String? 
	var taskList: [TaskList]? 
	var lastname: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		createdDatetime <- map["created_datetime"] 
		updateFormId <- map["update_form_id"] 
		jobId <- map["job_id"] 
		jobName <- map["job_name"] 
		companyName <- map["company_name"] 
		firstname <- map["firstname"] 
		userId <- map["user_id"] 
		taskList <- map["task_list"] 
		lastname <- map["lastname"] 
	}
} 

class TaskList: Mappable { 

	var tblTemplateFormId: String? 
	var qrCode: String? 
	var jobKey: String? 
	var jobId: String? 
	var formName: String? 
	var tblJobTaskSequenceId: String? 
	var sequence: String? 
    var isAnswerSubmitted : Bool = false
    
	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		tblTemplateFormId <- map["tbl_template_form_id"] 
		qrCode <- map["qr_code"] 
		jobKey <- map["job_key"] 
		jobId <- map["job_id"] 
		formName <- map["form_name"] 
		tblJobTaskSequenceId <- map["tbl_job_task_sequence_id"] 
		sequence <- map["sequence"] 
	}
} 

