//
//  CreateNewTempleteSecondViewModel.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class CreateNewTempleteSecondViewModel: AnswerDelegate {

    //MARK:- Variables
    fileprivate weak var theController:CreateNewTempleteSecondVC!
    
    init(theController:CreateNewTempleteSecondVC) {
        self.theController = theController
    }
    
    var questionsModel: [QuestionModel] = [QuestionModel]()
    var existingQuestionsModel :ExistingTemplatelistModelClass?
    var isEdit:Bool = false
    var formId:String = ""
    var formName:String = ""
    var formQRCode:String = ""
    
    func AnswerFinish(text: String, tag: Int, index:Int) {
        let data = self.questionsModel[index]
        data.answer![tag].answer = text
        self.questionsModel[index] = data
    }
    
    func ControlFinish(tag: Int, index: Int) {
        let data = self.questionsModel[index]
        if data.type == 0 {
            for value in data.answer! {
                value.selected = false
            }
            data.answer![tag].selected = true
        }
        else {
            data.answer![tag].selected = !data.answer![tag].selected!
        }
        self.questionsModel[index] = data
        self.theController.mainView.tableView.reloadData()
    }
    
}
