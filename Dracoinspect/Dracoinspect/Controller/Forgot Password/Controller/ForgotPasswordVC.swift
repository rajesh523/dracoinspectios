//
//  ForgotPasswordVc.swift
//  Draco Support
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ForgotPasswordVC: UIViewController {
    
    //MARK:- Variables
    lazy var forgotPasswordView: ForgotPasswordView = { [unowned self] in
        return self.view as! ForgotPasswordView
        }()
    
    lazy var forgotPasswordModelView: ForgotPasswordViewModel = {
        return ForgotPasswordViewModel(theController: self)
    }()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Functions
    func setupUI() {
        forgotPasswordView.setupUI(theDelegate: self)
    }
}
