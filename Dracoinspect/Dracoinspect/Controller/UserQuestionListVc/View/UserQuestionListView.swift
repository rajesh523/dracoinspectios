//
//  UserQuestionListView.swift
//  Dracoinspect
//
//  Created by Haresh on 06/06/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class UserQuestionListView: UIView {

    //MARK: - Outlet
    
    @IBOutlet weak var tblQuestionlist: UITableView!
    
    func setupUI()
    {
        tblQuestionlist.register(UINib(nibName: "QueWithQRCodeTblCell", bundle: nil), forCellReuseIdentifier: "QueWithQRCodeTblCell")
        tblQuestionlist.tableFooterView = UIView()
        
//        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
//        
//        tblModifyJob.addSubview(upperReferesh)
    }
    
    func reloadQuestiontableData()
    {
        self.tblQuestionlist.reloadData()
    }
    
    
}
