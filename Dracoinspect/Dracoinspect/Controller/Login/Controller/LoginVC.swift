//
//  LoginVC.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class LoginVC: UIViewController { 
    
    //MARK:- Variables
    lazy var loginView: LoginView = { [unowned self] in
        return self.view as! LoginView
        }()
    
    lazy var loginModelView: LoginViewModel = {
        return LoginViewModel(theController: self)
    }()
    
    //MARK: - View life cycle    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK:- Functions
    func setupUI() {
        loginView.setupUI(theDelegate: self)
    }    
}
