//
//  CreateNewTempleteVC-Tableview+Extension.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

extension CreateNewTempleteVC: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        if mainViewModel.isEdit && self.mainViewModel.questionsModel.isEmpty {
            return 0
        }
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 2 ? self.mainViewModel.questionCount : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell: FormNameCell = self.mainView.tableView.dequeueReusableCell(withIdentifier: "FormNameCell") as! FormNameCell
            cell.selectionStyle = .none
            cell.setupData(theDelegate: self)
            return cell
        }
        else if indexPath.section == 1 {
            let cell: HeaderCell = self.mainView.tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell
            return cell
        }
        else if indexPath.section == 2 {
            let data = self.mainViewModel.questionsModel[indexPath.row]
            
            let cell: QuestionsCell = self.mainView.tableView.dequeueReusableCell(withIdentifier: "QuestionsCell") as! QuestionsCell
            
            if self.mainViewModel.isEdit == true
            {
                cell.setupDataForEdit(theDelegate: self, index: indexPath.row, data: data)
            }
            else
            {
                cell.setupData(theDelegate: self, index: indexPath.row, data: data)
            }
            
            return cell
        }
        else {
            let cell: AddMoreQuestionCell = self.mainView.tableView.dequeueReusableCell(withIdentifier: "AddMoreQuestionCell") as! AddMoreQuestionCell
            cell.selectionStyle = .none
            return cell
        }
    }
}
