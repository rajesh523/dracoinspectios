//
//  arrangeAndDeleteTblCell.swift
//  Dracoinspect
//
//  Created by Haresh on 28/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class arrangeAndDeleteTblCell: MGSwipeTableCell,MGSwipeTableCellDelegate {
    
    //MARK: - Outlet
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblJobName: UILabel!
    
    @IBOutlet weak var vwMain: UIView!
    
    var handlerForDelete : (Int) -> Void = {_ in}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.vwMain.layer.borderColor = UIColor.black.cgColor
        self.vwMain.layer.borderWidth = 0.5
        self.vwMain.layer.cornerRadius = 4
        self.vwMain.layer.masksToBounds = true
        
        self.lblJobName.font = themeFont(size: 15, fontname: .regular)
        self.showsReorderControl = true
      
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setupData(data: AnyObject,indexPath:IndexPath)
    {
        
        lblNumber.text = "\(indexPath.row+1)."
        
        if let setData = data as? TempletList
        {
             self.lblJobName.text = setData.formName
        }
        else
        {
            self.lblJobName.text = (data as? TaskListExistingJob)?.formName
        }
       
        self.tag = indexPath.row
        
        self.rightButtons = [MGSwipeButton(title: "", icon: UIImage(named:"ic_delete_icon"), backgroundColor: UIColor.appthemeRedColor, callback: { (cell) -> Bool in
            
            print("self.tag:\(cell.tag)")
            print("self.tag:\(self.tag)")
            self.handlerForDelete(self.tag)
            return true
        })]
        self.rightSwipeSettings.transition = .rotate3D
        
    }
    
    
    func setupDataForExistingJob(data: TaskListExistingJob,indexPath:IndexPath)
    {
        self.lblJobName.text = data.formName
        self.tag = indexPath.row
        
        self.rightButtons = [MGSwipeButton(title: "", icon: UIImage(named:"ic_delete_icon"), backgroundColor: UIColor.appthemeRedColor, callback: { (cell) -> Bool in
            
            print("self.tag:\(cell.tag)")
            print("self.tag:\(self.tag)")
            self.handlerForDelete(self.tag)
            return true
        })]
        self.rightSwipeSettings.transition = .rotate3D
        
    }
    
}
