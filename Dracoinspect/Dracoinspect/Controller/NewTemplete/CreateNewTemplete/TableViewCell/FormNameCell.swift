//
//  FormNameCell.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 17/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

protocol FormNameDelegate: class {
    func FormNameFinish(text: String)
}

class FormNameCell: UITableViewCell, UITextFieldDelegate {

    weak var theController:CreateNewTempleteVC!

    weak var delegate:FormNameDelegate?
    
    @IBOutlet weak var viewQrCodeHeight: NSLayoutConstraint!
    @IBOutlet weak var txtFormName: UITextField!
    
    @IBOutlet weak var viewQrCode: UIView!
    @IBOutlet weak var lblCode: UILabel!
    
    @IBOutlet weak var heightTxtFormName: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.txtFormName.delegate = self

        self.txtFormName.font = themeFont(size: 17, fontname: .regular)
        self.lblCode.font = themeFont(size: 17, fontname: .regular)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)            
            self.delegate?.FormNameFinish(text: updatedText)
        }
        return true
    }
    
    func setupData(theDelegate: CreateNewTempleteVC) {
        self.theController = theDelegate
        self.delegate = theController.mainViewModel
        if self.theController.mainViewModel.isEdit {
            self.txtFormName.text = self.theController.mainViewModel.txtFormName
            let Code = self.theController.mainViewModel.formQRCode
            if Code == "" {
                self.viewQrCode.isHidden = true
                self.viewQrCodeHeight.constant = 0
            }
            else {
                self.viewQrCode.isHidden = false
                self.lblCode.text = Code
                self.viewQrCodeHeight.constant = 20
            }
        }
        else {
            self.txtFormName.text = self.theController.loadFormName() == "" ? self.theController.mainViewModel.txtFormName : self.theController.loadFormName()
            
            if theController.loadQRCode() == "" {
                self.viewQrCode.isHidden = true
                self.viewQrCodeHeight.constant = 0
            }
            else {
                self.viewQrCode.isHidden = false
                self.lblCode.text = theController.loadQRCode()
                self.viewQrCodeHeight.constant = 20
            }
        }
    }

}


class HeaderCell: UITableViewCell {
    weak var theController:CreateNewTempleteFinishVC!

    @IBOutlet weak var lblFormName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblFormName.font = themeFont(size: 17, fontname: .regular)
        // Initialization code
    }
    
    func setupData(theDelegate: CreateNewTempleteFinishVC) {
        self.theController = theDelegate
        if self.theController.mainViewModel.isEdit {
            self.lblFormName.text = self.theController.mainViewModel.formName
        }
        else {
            self.lblFormName.text = self.theController.loadFormName()
        }
    }
}
