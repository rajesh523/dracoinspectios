//
//  CreateJobSequenceViewModel.swift
//  Dracoinspect
//
//  Created by Haresh on 28/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class CreateJobSequenceViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:CreateJobSequenceVc!
    
    init(theController:CreateJobSequenceVc) {
        self.theController = theController
    }
    
    var arrayMainList : [AnyObject]?
    var strJobName = ""
    
    var deletedJobID = ""
    
    //MARK: - API calling
    
    func getJobListAPICalling()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(basicAdminURL)\(apiJobURL)\(createURL)"
            
            print("URL: \(url)")
            
            var arrayFinalArray : [JSON] = []
            
            for i in 0..<self.arrayMainList!.count
            {
                var dict = JSON()
                dict["sequence"].stringValue = "\(i+1)"
                
                //MainList as TempletList(For create) and TaskListExistingJob(Modify)
                
                if let array = self.arrayMainList as? [TempletList]
                {
                    dict["tbl_template_form_id"].stringValue = (self.arrayMainList as! [TempletList])[i].tblTemplateFormId ?? ""
                    
                }
                else
                {
                    dict["tbl_template_form_id"].stringValue = (self.arrayMainList as! [TaskListExistingJob])[i].tblTemplateFormId ?? ""
                }
                
                arrayFinalArray.append(dict)
            }
            
            let param : [String:String] = [
                "user_id" : getUserDetail("id"),
                "job_name" : self.strJobName,
                "job_sequence" : JSON(arrayFinalArray).rawString() ?? ""
                ]
            
            print("param :\(param)")
            
            theController.showLoader()
            
            CommonService().PostService(url: url, isLogin: false, param: param) { (response) in
                self.theController.hideLoader()
                
                if let json = response.value
                {
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        print("JSON : \(json)")
                        
                        if let data = json.dictionaryObject
                        {
                            self.theController.navigationController?.popViewController(animated: true)
                        }
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        
                    }
                    else
                    {
                        
                         makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
                
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    
    //MARK: - Exsting Job API calling
    
    func existingJobAPICalling(selectedJobID:String,isRefreshing:Bool,failed: @escaping (String) -> Void)
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(basicAdminURL)\(apiJobURL)\(existingJobURL)"
            
            print("URL: \(url)")
            
            var param = [
                "user_id" : getUserDetail("id"),
                "job_id" : theController.selectedJobID
                ]
            
            print("param :\(param)")
            
            if !isRefreshing
            {
                theController.showLoader()
            }
            
            
            CommonService().PostService(url: url, isLogin: false, param: param) { (response) in
                
                if !isRefreshing
                {
                    self.theController.hideLoader()
                }
                
                (self.theController.view as? CreateJobSequenceDetailsView)?.stopUpperRefresh()
                
                if let json = response.value
                {
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        print("JSON : \(json)")
                        
                        if let data = json.dictionaryObject
                        {
                            if let list = ExistingJobModel(JSON: data)
                            {

                                self.arrayMainList?.removeAll()
                                self.arrayMainList = []
                                
                                self.arrayMainList = list.data?[0].taskList
                                
                                 (self.theController.view as? CreateJobSequenceView)?.txtEnterForm.text = list.data?[0].jobName
                                
                            }
                            
                            (self.theController.view as? CreateJobSequenceView)?.reloadTable()
                            
                        }
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        
                    }
                    else
                    {
                        failed(json["msg"].stringValue)
                        // makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
                
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }

    
    func updateJobList()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(basicAdminURL)\(apiJobURL)\(updateJobURL)"
            
            print("URL: \(url)")
            
            var arrayFinalArray : [JSON] = []
            
            for i in 0..<self.arrayMainList!.count
            {
                var dict = JSON()
                dict["sequence"].stringValue = "\(i+1)"
                
                //MainList as TempletList(For create) and TaskListExistingJob(Modify)
                
                if let array = self.arrayMainList?[i] as? TempletList
                {
                    dict["tbl_template_form_id"].stringValue = array.tblTemplateFormId ?? ""
                    dict["tbl_job_task_sequence_id"].stringValue = "new"
                    
                }
                else
                {
                    
                    if let data = self.arrayMainList?[i] as? TaskListExistingJob
                    {
                        dict["tbl_template_form_id"].stringValue = data.tblTemplateFormId ?? ""
                        dict["tbl_job_task_sequence_id"].stringValue = data.tblJobTaskSequenceId ?? ""
                    }
                    
                }
                
                arrayFinalArray.append(dict)
            }
            
            let param = [
                "user_id" : getUserDetail("id"),
                "job_id" : theController.selectedJobID,
                "job_name" : self.strJobName,
                "delete_template_form_id" : deletedJobID,
                "job_sequence" : JSON(arrayFinalArray).rawString() ?? ""
            ]
            
            print("param :\(param)")
            
            self.theController.showLoader()
            
            CommonService().PostService(url: url, isLogin: false, param: param) { (response) in
                
                self.theController.hideLoader()
                
                if let json = response.value
                {
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        print("JSON : \(json)")
                        
                        if let data = json.dictionaryObject
                        {
                            self.theController.navigationController?.popViewController(animated: true)
                            
                            makeToast(strMessage: json["msg"].stringValue)
                            
                        }
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        
                    }
                    else
                    {
                         makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
                
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    
}
