//
//  ModifyJobTblCell.swift
//  Dracoinspect
//
//  Created by Haresh on 31/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class ModifyJobTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: CustomView!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblJob: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.lblJob.font = themeFont(size: 15, fontname: .regular)
        
        self.lblNumber.textColor = UIColor.appthemeRedColor
        self.lblNumber.layer.borderColor = UIColor.appthemeRedColor.cgColor
        self.lblNumber.layer.borderWidth = 0.8
        self.lblNumber.layer.masksToBounds = true
        
    //    self.lblNumber.textColor = UIColor.white
        self.lblNumber.font = themeFont(size: 13, fontname: .semibold)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setupData(data : ModifyList,index : Int)
    {
       
         //self.lblNumber.text = "\(index+1)"
        
        self.lblJob.text = data.jobName
    }
    
    
}
