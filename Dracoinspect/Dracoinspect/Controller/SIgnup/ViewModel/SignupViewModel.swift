//
//  SignupViewModel.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import SwiftyJSON
import DropDown
import Alamofire
import FirebaseMessaging

class SignupViewModel {

    //MARK:- Variables
    fileprivate weak var theController:SignupVC!
    
    init(theController:SignupVC) {
        self.theController = theController
    }
    
    //MARK: - Variable    
    var companyNameDD = DropDown()
    var strCompnayID = ""
    
    var arrayCompanyName : [JSON] = []
    var arrayCompanyNameString = [String]()
    
    var arrayBatchList : [JSON] = []
    var arrayBatchListString = [String]()
    var batchListCheck = Bool()
    
    var QRCodeCheck = Bool()
    
    var arrayQRCodeString = [String]()
    var arrayData : [JSON] = []
    
    func selectionIndex()
    {
        
        self.companyNameDD.selectionAction = { (index, item) in
            self.theController.signUpView.txtCompanyName.text = item
            self.theController.view.endEditing(true)
            self.strCompnayID = (self.arrayCompanyName[index]["id"]).stringValue
            
            self.companyNameDD.hide()
            
        }
    }
    
    func signUpAPICalling()
    {
        self.theController.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(userURL)\(registerURL)"
            
            print("URL: \(url)")
            
            let param = ["lang" : GlobalVariables.strLang,
                         "email" : self.theController.signUpView.txtEmail.text ?? "",
                         "password" : self.theController.signUpView.txtPassword.text ?? "",
                         "mobile" : self.theController.signUpView.txtPhoneNumber.text ?? "",
                         "firstname" : self.theController.signUpView.txtfirstName.text ?? "",
                         "lastname" : self.theController.signUpView.txtLastName.text ?? "",
                         "timezone" : GlobalVariables.localTimeZoneName,
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? "",
                         "register_id" : Messaging.messaging().fcmToken ?? "",
                "device_type" : GlobalVariables.deviceType,
                "company_id" : strCompnayID,
                "othorize_code" : self.theController.signUpView.txtAuthenticationCode.text ?? ""
            ]
            
            print("param :\(param)")
            
            self.theController.showLoader()
            
            CommonService().PostService(url: url,isLogin:true, param: param) { (respones) in
                
                self.theController.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        
                        makeToast(strMessage: json["msg"].stringValue)
                        
                        self.theController.navigationController?.popViewController(animated: true)
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        // self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func getCompanyOrganizationList()
    {
        self.theController.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(userURL)\(adminListURL)"
            
            print("URL: \(url)")
            
            let param = [String:String]()
            
            print("param :\(param)")
            
            self.theController.showLoader()
            
            CommonService().PostService(url: url,isLogin:true, param: param) { (respones) in
                
                self.theController.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"].arrayValue
                        
                        self.arrayCompanyName = data
                        
                        self.arrayCompanyNameString = data.map({"\($0["company_name"].stringValue)"})
                        self.companyNameDD.dataSource = self.arrayCompanyNameString
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        // self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }

}
