
//
//  ModifyJobSequenceViewModel.swift
//  Dracoinspect
//
//  Created by Haresh on 31/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class ModifyJobSequenceViewModel
{

    //MARK:- Variables
    fileprivate weak var theController:ModifyJobSequenceVc!
    
    init(theController:ModifyJobSequenceVc) {
        self.theController = theController
    }
    
    var arrayModifyList : [ModifyList]?
    
    
    func callModifyList(isRefreshing:Bool,failed: @escaping (String) -> Void)
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(basicAdminURL)\(apiJobURL)\(jobListURL)"
            
            print("URL: \(url)")
            
            var param = [
                "user_id" : getUserDetail("id"),
                ]
            
            print("param :\(param)")
            
            if !isRefreshing
            {
                theController.showLoader()
            }
            
            CommonService().PostService(url: url, isLogin: false, param: param) { (response) in
                
                if !isRefreshing
                {
                    self.theController.hideLoader()
                }
                
                (self.theController.view as? ModifyJobSequenceView)?.stopUpperRefresh()
                
                if let json = response.value
                {
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        print("JSON : \(json)")
                        
                        if let data = json.dictionaryObject
                        {
                            if let list = ModifyJobSequenceModel(JSON: data)
                            {
                                self.arrayModifyList?.removeAll()
                                self.arrayModifyList = []

                                self.arrayModifyList = list.data
                                
                            }
                            
                            (self.theController.view as? ModifyJobSequenceView)?.tableReload()
                            
                        }
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        
                    }
                    else
                    {
                         failed(json["msg"].stringValue)
                         makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
                
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
}
