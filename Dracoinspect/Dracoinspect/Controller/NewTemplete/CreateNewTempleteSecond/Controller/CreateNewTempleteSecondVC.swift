//
//  CreateNewTempleteSecondVC.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 17/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class CreateNewTempleteSecondVC: UIViewController  {

    //MARK:- Variables
    lazy var mainView: CreateNewTempleteSecondView = { [unowned self] in
        return self.view as! CreateNewTempleteSecondView
        }()
    
    lazy var mainViewModel: CreateNewTempleteSecondViewModel = {
        return CreateNewTempleteSecondViewModel(theController: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Create_a_new_task_key"))
        self.setupUI()
        // Do any additional setup after loading the view.
    }

    func setupUI() {
        mainView.setupUI(theDelegate: self)
    }
}
