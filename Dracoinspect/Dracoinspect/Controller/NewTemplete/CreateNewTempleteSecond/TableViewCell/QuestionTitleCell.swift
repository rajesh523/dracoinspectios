//
//  QuestionTitleCell.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 17/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

protocol AnswerDelegate: class {
    func AnswerFinish(text: String, tag:Int, index:Int)
    func ControlFinish(tag:Int, index:Int)
}

class QuestionTitleCell: UITableViewCell {

    @IBOutlet weak var txtQuestion: UITextField!
    @IBOutlet weak var lblQuestionNo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.txtQuestion.font = themeFont(size: 15, fontname: .regular)
        self.lblQuestionNo.font = themeFont(size: 15, fontname: .regular)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupDataFinish(indexPath:IndexPath, data: QuestionModel) {
        self.lblQuestionNo.text = "Q\(indexPath.section)."
        self.txtQuestion.text = data.question
        self.txtQuestion.isUserInteractionEnabled = false
    }
    
    func setupData(indexPath:IndexPath, data: QuestionModel) {
        self.lblQuestionNo.text = "Q\(indexPath.section + 1)."
        self.txtQuestion.text = data.question
        self.txtQuestion.isUserInteractionEnabled = false
    }
}


class QuestionAnswerCell: UITableViewCell, UITextFieldDelegate {
    
    weak var theController:CreateNewTempleteSecondVC!

    @IBOutlet weak var txtAnswer: UITextField!
    @IBOutlet weak var btnControl: UIButton!
    weak var delegate: AnswerDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtAnswer.delegate = self
        
        self.txtAnswer.font = themeFont(size: 13, fontname: .regular)
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            self.delegate?.AnswerFinish(text: updatedText, tag: self.txtAnswer.tag, index: self.tag)
        }
        return true
    }
    
    
    @IBAction func btnControllClicked(_ sender: Any) {
        self.delegate?.ControlFinish(tag: self.txtAnswer.tag, index: self.tag)
    }
    
    func setupData(theDelegate: CreateNewTempleteSecondVC, indexPath:IndexPath, data: QuestionModel) {
        let answer = data.answer
        self.delegate = theDelegate.mainViewModel
        let dict = answer![indexPath.row - 1]
        let answerDict = dict.answer
        self.txtAnswer.text = answerDict
        self.txtAnswer.tag = indexPath.row - 1
        self.tag = indexPath.section
        self.setImage(cell: self, data: dict, type: data.type!)
    }
    
    func setImage(cell: QuestionAnswerCell, data:AnswerModel, type:Int) {
        if type == 1 {
            cell.btnControl.setBackgroundImage(UIImage(named: data.selected! ? "ic_redio_button_on" : "ic_redio_button_off"), for: .normal)
        }
        else if type == 2 {
            cell.btnControl.setBackgroundImage(UIImage(named: data.selected! ? "ic_check_icon_on" : "ic_check_icon_off"), for: .normal)
        }
        else {
            cell.btnControl.setBackgroundImage(UIImage(named: ""), for: .normal)
        }
    }
    
}

class AddAnswerCell: UITableViewCell {
    
    weak var theController:CreateNewTempleteSecondVC!

    @IBOutlet weak var btnAddMoreHeight: NSLayoutConstraint!
    @IBOutlet weak var btnAddMore: CustomButton!
    @IBOutlet weak var lblType: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnAddMore.titleLabel?.font = themeFont(size: 15, fontname: .regular)
        self.lblType.font = themeFont(size: 15, fontname: .regular)

        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupData(theDelegate: CreateNewTempleteSecondVC, indexPath:IndexPath, data: QuestionModel) {
        self.theController = theDelegate
        for (index, type) in DropDownQuestionTypeId.enumerated() {
            if type == data.type! {
                self.lblType.text = DropDownQuestion[index]
            }
        }
        self.btnAddMore.tag = indexPath.section
        if data.type! == 3 || data.type! == 5 {
            self.btnAddMoreHeight.constant = 0
        }
        else {
            self.btnAddMoreHeight.constant = 26
        }
    }
}
