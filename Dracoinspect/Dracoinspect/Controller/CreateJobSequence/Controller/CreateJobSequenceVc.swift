//
//  CreateJobSequenceVc.swift
//  Dracoinspect
//
//  Created by Haresh on 28/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

enum checkCreateOrUpdateJob
{
    case create
    case modify
}


class CreateJobSequenceVc: UIViewController {

    //MARK:- Variables
    lazy var theView: CreateJobSequenceView = { [unowned self] in
        return self.view as! CreateJobSequenceView
        }()
    
    lazy var theViewModel: CreateJobSequenceViewModel = {
        return CreateJobSequenceViewModel(theController: self)
    }()
    
    var selectedController = checkCreateOrUpdateJob.create
    
    var selectedJobID = ""
    var strErrorMsg = ""
    //MARK: - View life cycle
    
    var strDeletedArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
        if selectedController == .create
        {
            //Not Calling API
        }
        else
        {
            //Calling API
            self.theViewModel.existingJobAPICalling(selectedJobID: selectedJobID, isRefreshing: false) { (error) in
                print("error:\(error)")
                self.strErrorMsg = error
            }
            
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if selectedController == .create
        {
            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Create_new_job_key"))
        }
        else if selectedController == .modify
        {
            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Modify_new_job_key"))
        }
        
    }
    
    func setupUI()
    {
        self.theView.setupUI()
    }

}

//MARK: - IBAction

extension CreateJobSequenceVc
{
    @IBAction func btnAddTaskForThisjobTapped(_ sender: UIButton) {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CreateJobSequenceDetailsVc") as! CreateJobSequenceDetailsVc
        
        var selectedTemplet = ""
        
        if self.theViewModel.arrayMainList != nil
        {
            
            var strAvailable = [String]()
            
            for item in self.theViewModel.arrayMainList!
            {
                if ((item as? TempletList) != nil)
                {
                    let value = (item as? TempletList)?.tblTemplateFormId
                    strAvailable.append(value ?? "")
                }
                else
                {
                    let value = (item as? TaskListExistingJob)?.tblTemplateFormId
                    strAvailable.append(value ?? "")
                }
                
            }
            
            print("strAvailable:\(strAvailable)")
            
            selectedTemplet = strAvailable.joined(separator: ",")
        }
        
        obj.handlerForArray = { [weak self] (array) in
            self?.appendArrayValue(array: array)
        }

        if selectedController == .create
        {
            obj.selectedController = .create
        }
        else
        {
            obj.selectedController = .modify
        }
       
        print("selectedTempet:\(selectedTemplet)")
        obj.selectedJob = selectedTemplet
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnFinishTapped(_ sender: UIButton) {
        
        
        if self.theView.checkValidation(theViewModel: self.theViewModel)
        {
            if selectedController == .create
            {
                self.theViewModel.getJobListAPICalling()
            }
            else if selectedController == .modify
            {
                //Modify method call
                self.theViewModel.updateJobList()
            }
        }

    }
    
    //MARK: - Array append
    
    func appendArrayValue(array : [AnyObject])
    {
        if self.theViewModel.arrayMainList == nil
        {
            self.theViewModel.arrayMainList = array
        }
        else
        {
            self.theViewModel.arrayMainList = self.theViewModel.arrayMainList! + array
        }
        
        self.theView.reloadTable()
    }
    
    //MARK: - Alert
    
    func showAlert(index: Int) {
        let alertController = UIAlertController(title: getCommonString(key: "Draco_Inspect_key"), message: getCommonString(key: "Are_you_sure_want_to_delete?_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            if self.theViewModel.arrayMainList != nil
            {
                
                if let _ = (self.theViewModel.arrayMainList?[index] as? TempletList)
                {
                    
                }
                else
                {
                    let deletedValue = (self.theViewModel.arrayMainList?[index] as? TaskListExistingJob)?.tblTemplateFormId
                    self.strDeletedArray.append(deletedValue!)
                    self.theViewModel.deletedJobID = self.strDeletedArray.joined(separator: ",")
                }
                
                self.theViewModel.arrayMainList?.remove(at: index)
                self.theView.reloadTable()
            }
            
        }
        
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension CreateJobSequenceVc: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if theViewModel.arrayMainList != nil && theViewModel.arrayMainList?.count != 0
        {
            self.theView.btnFinish(isHidden: false)
            tableView.backgroundView = nil
            return theViewModel.arrayMainList?.count ?? 0
        }
        else
        {
            let lbl = UILabel()
            lbl.text = "Click add more to add task to job"
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appthemeRedColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            self.theView.btnFinish(isHidden: true)
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "arrangeAndDeleteTblCell") as! arrangeAndDeleteTblCell
        
        cell.setupData(data: theViewModel.arrayMainList![indexPath.row],indexPath:indexPath)
        cell.handlerForDelete = { [weak self] (selectedIndex) in
            
            print("selectedIndex:\(selectedIndex)")
            self?.showAlert(index: selectedIndex)
        }

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let moveObject = self.theViewModel.arrayMainList![sourceIndexPath.row]
        self.theViewModel.arrayMainList!.remove(at: sourceIndexPath.row)
        self.theViewModel.arrayMainList!.insert(moveObject, at: destinationIndexPath.row)
        
         self.theView.reloadTable()
        
        /*
        if selectedController == .create
        {
            let moveObject = self.theViewModel.arrayMainList![sourceIndexPath.row]
            self.theViewModel.arrayMainList!.remove(at: sourceIndexPath.row)
            self.theViewModel.arrayMainList!.insert(moveObject, at: destinationIndexPath.row)
        }
        else if selectedController == .modify
        {
            let moveObject = self.theViewModel.arrayExistingJobList![sourceIndexPath.row]
            self.theViewModel.arrayExistingJobList!.remove(at: sourceIndexPath.row)
            self.theViewModel.arrayExistingJobList!.insert(moveObject, at: destinationIndexPath.row)
            
        }
        */
    }
    
    
    
}




