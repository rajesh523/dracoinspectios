//
//  QueAndAnswerTblCell.swift
//  Dracoinspect
//
//  Created by Haresh on 06/06/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class QueAndAnswerTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var lblQuestiuon: UILabel!
    @IBOutlet weak var tblInnerAnswer: UITableView!
    @IBOutlet weak var heightConstantInnerTbl: NSLayoutConstraint!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tblInnerAnswer.register(UINib(nibName: "CheckMarkTblCell", bundle: nil), forCellReuseIdentifier: "CheckMarkTblCell")
        tblInnerAnswer.register(UINib(nibName: "RadioBtnTblCell", bundle: nil), forCellReuseIdentifier: "RadioBtnTblCell")
        tblInnerAnswer.register(UINib(nibName: "EnterQRCodeTxtfldTblCell", bundle: nil), forCellReuseIdentifier: "EnterQRCodeTxtfldTblCell")
        tblInnerAnswer.register(UINib(nibName: "txtvwAnswerTblCell", bundle: nil), forCellReuseIdentifier: "txtvwAnswerTblCell")
        tblInnerAnswer.register(UINib(nibName: "DropDownTblCell", bundle: nil), forCellReuseIdentifier: "DropDownTblCell")
        
        
        tblInnerAnswer.tableFooterView = UIView()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupData(data:QAQuestions,index:Int)
    {
        self.lblQuestiuon.text = "Q\(index+1)." + " " + (data.questionsName ?? "")
    }
    
}
    

