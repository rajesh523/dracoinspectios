

//
//  MainVC.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 20/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class MainVC: UIViewController {
    
    //MARK:- Variables
    lazy var mainView: MainView = { [unowned self] in
        return self.view as! MainView
        }()
    
    lazy var mainModelView: MainViewModel = {
        return MainViewModel(theController: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Draco_Inspect_key"), isRightButtonHidden: false)
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK:- Functions
    func setupUI() {
        mainView.setupUI(theDelegate: self)
    }
}
