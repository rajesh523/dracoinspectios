//
//  UseAnExistingTempleteVC-.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

extension UseAnExistingTempleteVC: UITableViewDelegate, UITableViewDataSource, MGSwipeTableCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.mainView.lblNotFound.isHidden = self.mainModelView.taskList?.data?.count ?? 0 != 0
        return self.mainModelView.taskList?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: FormNameListCell = self.mainView.tableView.dequeueReusableCell(withIdentifier: "FormNameListCell") as! FormNameListCell
        cell.selectionStyle = .none
        cell.setupData(theDelegate: self, indexPath: indexPath, data: (self.mainModelView.taskList?.data?[indexPath.row])!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = (self.mainModelView.taskList?.data?[indexPath.row])!
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CreateNewTempleteVC") as! CreateNewTempleteVC
        obj.mainViewModel.isEdit = true
        obj.mainViewModel.formId = data.tblTemplateFormId!
        obj.mainViewModel.getExistingTemplateListAPICalling()
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
