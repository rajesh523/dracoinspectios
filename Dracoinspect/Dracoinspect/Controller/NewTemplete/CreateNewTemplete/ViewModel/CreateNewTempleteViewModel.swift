//
//  CreateNewTempleteViewModel.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CreateNewTempleteViewModel: QuestionDelegate, FormNameDelegate {
    
    //MARK:- Variables
    fileprivate weak var theController:CreateNewTempleteVC!
    
    init(theController:CreateNewTempleteVC) {
        self.theController = theController
    }
    
    var questionCount:Int = 1
    var txtFormName:String = ""
    var questionsModel: [QuestionModel] = [QuestionModel]()
    var existingQuestionsModel :ExistingTemplatelistModelClass?
    
    var isEdit:Bool = false
    var formId:String = ""
    var formQRCode:String = ""
    
    func QuestionFinish(text: String, selectedType: Int, tag:Int) {
        let data = self.questionsModel[tag]
        data.question = text
        data.type = selectedType
        self.questionsModel[tag] = data
    }
    
    func FormNameFinish(text: String) {
        self.txtFormName = text
    }
    
    func getExistingTemplateListAPICalling() {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(basicAdminURL)\(apiQuestionsURL)\(getExistingTemplateListURL)"
            print("URL: \(url)")
            let param = ["user_id" : getUserDetail("id"), "tbl_template_form_id": self.formId]
            print("param :\(param)")
            self.theController.showLoader()
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                self.theController.hideLoader()
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        let jsonModel = ExistingTemplatelistModelClass(JSON: json.dictionaryObject!)
                        self.existingQuestionsModel = jsonModel
                        self.txtFormName = (jsonModel?.data?.formDetail?.first?.formName)!
                        self.txtFormName = (jsonModel?.data?.formDetail?.first?.formName)!
                        self.formQRCode = (jsonModel?.data?.formDetail?.first?.qrCode)!
                        
                        var questionsModelNew: [QuestionModel] = [QuestionModel]()
                        for data in (jsonModel?.data?.formDetail)! {
                            for dataValue in data.questions! {
                                var answerModelNew: [AnswerModel] = [AnswerModel]()
                                for ansValue in dataValue.optionList! {
                                    answerModelNew.append(AnswerModel.init(answer: ansValue.options, selected: false))
                                }
                                let model = QuestionModel.init(question: dataValue.questionsName, type: Int(dataValue.questionTypeId!), questionId: dataValue.questionsId, answer: answerModelNew)
                                questionsModelNew.append(model)
                            }
                        }
                        
                        print(self.theController.toNSMutableArray(model: questionsModelNew))
                        self.questionsModel = questionsModelNew
                        self.questionCount = self.questionsModel.count
                        self.theController.mainView.reloadData()
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
}
