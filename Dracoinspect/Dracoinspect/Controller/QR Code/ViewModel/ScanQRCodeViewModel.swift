//
//  ScanQRCodeViewModel.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import SwiftyJSON
import QRCodeReader
import Alamofire
import AlamofireSwiftyJSON

protocol ScanQRCodeDelegate: class {
    func ScanQRCode(code:String)
}

class ScanQRCodeViewModel {

    //MARK:- Variables
    fileprivate weak var theController:ScanQRCodeVC!
    
    init(theController:ScanQRCodeVC) {
        self.theController = theController
    }
    
    //MARK: - Variable
    
    var arrayCompanyList : [JSON] = []
    weak var delegate: ScanQRCodeDelegate?
    
    var strEnterQRCode = ""
    
    //MARK: - API calling
    
    func matchQRCode()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(basicAdminURL)\(apiJobURL)\(jobtaskQrcodeMatchURL)"
            
            print("URL: \(url)")
            
            var param = [
                "user_id" : getUserDetail("id"),
                "tbl_template_form_id" : theController.strTempletFormID,
                "qr_code" : self.strEnterQRCode
                ]
            
            print("param :\(param)")
            
            theController.showLoader()
            
            CommonService().PostService(url: url, isLogin: false, param: param) { (response) in
                
                self.theController.hideLoader()
                
                
                if let json = response.value
                {
                    
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        //Redirect to QuestionList screen
                        self.theController.redirectToAnswerList()
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.theController.navigationController?.popViewController(animated: true)
                    }
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
                
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    
    //MARK: - Other Action
    
    func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                            UIApplication.shared.openURL(settingsURL)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            
            theController.present(alert, animated: true, completion: nil)
            
            return false
        }
    }
    
    
    func redirectToSubmitDetails(qrScanData : String) {
        
        if theController.selectedController == .fromNewTemplet
        {
            self.theController.navigationController?.popViewController(animated: true)
            self.delegate?.ScanQRCode(code: qrScanData)
        }
        else if theController.selectedController == .submitAnswerUserSide // user enter code in answer
        {
            self.theController.navigationController?.popViewController(animated: true)
            self.delegate?.ScanQRCode(code: qrScanData)
        }
        else if theController.selectedController == .clickOnCellToCheckCodeUserSide
        {
            self.strEnterQRCode = qrScanData
            self.matchQRCode()
        }
        
    }    
}

