//
//  AnswerListCell.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 17/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import DropDown

class AnswerListCell: UITableViewCell {
    @IBOutlet weak var btnControl: UIButton!
    @IBOutlet weak var lblAnswer: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblAnswer.font = themeFont(size: 17, fontname: .regular)

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupData(indexPath:IndexPath, data: QuestionModel) {
        let answer = data.answer
        let dict = answer![indexPath.row - 1]
        let answerTitle = dict.answer
        self.lblAnswer.text = answerTitle
        self.selectionStyle = .none
        self.lblAnswer.tag = indexPath.row - 1
        self.tag = indexPath.section
        self.setImage(cell: self, data: dict, type: data.type!)
    }
    
    func setImage(cell: AnswerListCell, data:AnswerModel, type:Int) {
        if type == 1 {
            self.btnControl.setBackgroundImage(UIImage(named: data.selected! ? "ic_redio_button_on" : "ic_redio_button_off"), for: .normal)
        }
        else if type == 2 {
            self.btnControl.setBackgroundImage(UIImage(named: data.selected! ? "ic_check_icon_on" : "ic_check_icon_off"), for: .normal)
        }
        else {
            self.btnControl.setBackgroundImage(UIImage(named: ""), for: .normal)
        }
    }
}


class AnswerListDropDownCell: UITableViewCell {
    
    @IBOutlet weak var lblQueNo: UILabel!
    @IBOutlet weak var txtQuestion: UITextField!
    
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var lblDropDownTitle: UILabel!
    
    let rightBarDropDown = DropDown()
    weak var delegate:QuestionDelegate?
    var selectedType:Int = -1
    
    var DropDownAnswer:[String] = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblDropDownTitle.font = themeFont(size: 15, fontname: .regular)

        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setDropDown() {
        rightBarDropDown.anchorView = btnSelect
        rightBarDropDown.dataSource = DropDownAnswer
        rightBarDropDown.cellConfiguration = { (index, item) in return "\(item)" }
        
        rightBarDropDown.selectionAction = { (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.selectedType = index
            self.lblDropDownTitle.text = self.DropDownAnswer[index]
            self.lblDropDownTitle.textColor = .darkGray
            self.delegate?.QuestionFinish(text: self.txtQuestion.text!, selectedType: self.selectedType, tag: self.tag)
        }
        rightBarDropDown.backgroundColor = .white
        rightBarDropDown.width = btnSelect.frame.width - 35
        rightBarDropDown.bottomOffset = CGPoint(x: 0, y:(rightBarDropDown.anchorView?.plainView.bounds.height)!)
    }
    
    @IBAction func btnSelectClicked(_ sender: Any) {
        rightBarDropDown.show()
    }
    
    func setupData(indexPath:IndexPath, data: QuestionModel) {
        var DropDownAnswer:[String] = [String]()
        self.tag = indexPath.row
        for dict in data.answer! {
            DropDownAnswer.append(dict.answer!)
        }
        self.DropDownAnswer = DropDownAnswer
        self.setDropDown()
    }
}
