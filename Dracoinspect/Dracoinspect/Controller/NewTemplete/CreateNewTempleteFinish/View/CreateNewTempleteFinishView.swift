//
//  CreateNewTempleteFinishView.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import SwiftyJSON

class CreateNewTempleteFinishView: UIView {

    //MARK:- Variables
    fileprivate weak var theController: CreateNewTempleteFinishVC?
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnFinish: CustomButton!
    
    func setupUI(theDelegate: CreateNewTempleteFinishVC) {
        self.theController = theDelegate
        btnFinish.setThemeButtonUI()
        btnFinish.setTitle(getCommonString(key: "Finish_key"), for: .normal)
    }

    @IBAction func btnBackClicked(_ sender: Any) {
        self.theController?.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFinishClicked(_ sender: Any) {
        var isAnswerAdded:Bool = true
        for data in (self.theController?.mainViewModel.questionsModel)! {
            for dataValue in data.answer! {
                let answer = dataValue.answer
                if answer == "" {
                    isAnswerAdded = false
                }
            }
        }
        
        if !isAnswerAdded {
            makeToast(strMessage: getCommonString(key:"Please_fill_all_fields"))
            return
        }
        
        if !(self.theController?.mainViewModel.isEdit)! {
            self.theController?.mainViewModel.questionsAnswerAPICalling()
        }
        else {
            self.theController?.mainViewModel.existingTemplateFinishAPICalling()
        }
    }
}
