//
//  ModifyJobSequence.swift
//  Dracoinspect
//
//  Created by Haresh on 31/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class ModifyJobSequenceVc: UIViewController {

    //MARK: - Variable
    
    lazy var theView: ModifyJobSequenceView = { [unowned self] in
        return self.view as! ModifyJobSequenceView
        }()
    
    lazy var theViewModel: ModifyJobSequenceViewModel = {
        return ModifyJobSequenceViewModel(theController: self)
    }()
    
    
    
    var strErrorMessage = ""
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()

        NotificationCenter.default.addObserver(self, selector: #selector(startUpperRefresh), name: NSNotification.Name(rawValue: "reloadMainJobListList"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
        if GlobalVariables.isUserLogin == false
        {
            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Existing_job_list_key"))
        }
        else
        {
            setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Existing_job_list_key"), isRightButtonHidden: false)
        }
        
    }
    
    func setupUI()
    {
        self.theView.setupUI()
        
        self.theViewModel.callModifyList(isRefreshing: false) { (error) in
            self.strErrorMessage = error
        }
       
    }

    @objc func startUpperRefresh()
    {
        self.theViewModel.callModifyList(isRefreshing: true) { (error) in
            self.strErrorMessage = error
        }
    }
    
}

//MARK: - TableView dataSource and delegate


extension ModifyJobSequenceVc: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if theViewModel.arrayModifyList?.count == 0 || theViewModel.arrayModifyList == nil
        {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appthemeRedColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        
        tableView.backgroundView = nil
        return theViewModel.arrayModifyList?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ModifyJobTblCell") as! ModifyJobTblCell
        
        cell.setupData(data: theViewModel.arrayModifyList![indexPath.row], index: indexPath.row)
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if GlobalVariables.isUserLogin == false
        {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "CreateJobSequenceVc") as! CreateJobSequenceVc
            obj.selectedJobID = theViewModel.arrayModifyList![indexPath.row].jobId!
            obj.selectedController = .modify
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else
        {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "UserQuestionListVc") as! UserQuestionListVc
            obj.strJobName = theViewModel.arrayModifyList?[indexPath.row].jobName ?? ""
            obj.strJobId = theViewModel.arrayModifyList?[indexPath.row].jobId ?? ""
            obj.theViewModel.arrayTaskList = theViewModel.arrayModifyList?[indexPath.row].taskList
            self.navigationController?.pushViewController(obj, animated: true)
            
        }
        
    }
    
}
