//
//  AnswerListView.swift
//  Dracoinspect
//
//  Created by Haresh on 06/06/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class AnswerListView: UIView {

    //MARK: - Outlet
    @IBOutlet weak var tblAnswerList: UITableView!
    @IBOutlet weak var btnFinish: CustomButton!
    @IBOutlet weak var lblMainTaskName: UILabel!
    
    func setupUI()
    {
        tblAnswerList.tag = 100
        tblAnswerList.register(UINib(nibName: "QueAndAnswerTblCell", bundle: nil), forCellReuseIdentifier: "QueAndAnswerTblCell")
        tblAnswerList.tableFooterView = UIView()

    }
    
    func tableReload()
    {
        self.tblAnswerList.reloadData()
    }
    
}
