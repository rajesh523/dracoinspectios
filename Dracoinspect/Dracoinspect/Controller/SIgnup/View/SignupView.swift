//
//  SignupView.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class SignupView: UIView, UITextFieldDelegate {
    
    //MARK:- Variables
    fileprivate weak var theController: SignupVC?
    
    //MARK: - Outlet
    @IBOutlet weak var vwFirstName: CustomView!
    @IBOutlet weak var txtfirstName: CustomTextField!
    
    @IBOutlet weak var vwLastName: CustomView!
    @IBOutlet weak var txtLastName: CustomTextField!
    
    @IBOutlet weak var vwPhoneNumber: CustomView!
    @IBOutlet weak var txtPhoneNumber: CustomTextField!
    
    @IBOutlet weak var vwAuthenticationCode: CustomView!
    @IBOutlet weak var txtAuthenticationCode: CustomTextField!
    
    @IBOutlet weak var vwCompanyName: CustomView!
    @IBOutlet weak var txtCompanyName: CustomTextField!
    
    @IBOutlet weak var vwEmail: CustomView!
    @IBOutlet weak var txtEmail: CustomTextField!
    
    @IBOutlet weak var vwPassword: CustomView!
    @IBOutlet weak var txtPassword: CustomTextField!
    
    @IBOutlet weak var vwConfirmPassword: CustomView!
    @IBOutlet weak var txtConfirmPassword: CustomTextField!
    
    @IBOutlet weak var btnSignUp: CustomButton!    
    
    func setupUI(theDelegate: SignupVC)
    {
        self.theController = theDelegate
        [txtfirstName,txtLastName,txtPhoneNumber,txtCompanyName,txtEmail,txtPassword,txtConfirmPassword,txtAuthenticationCode].forEach { (txt) in
            txt?.delegate = self
            txt?.leftPaddingView = 55
            txt?.setThemeTextFieldUI()
            
        }
        
        [vwFirstName,vwLastName,vwPhoneNumber,vwCompanyName,vwEmail,vwPassword,vwConfirmPassword,vwAuthenticationCode].forEach { (vw) in
            vw?.cornerRadius = (vw!.bounds.height)/2
            vw?.borderWidth = 1.0
            vw?.borderColors = UIColor.appthemeRedColor
            vw?.shadowColors = UIColor.lightGray
            vw?.shadowRadius = 2.0
            vw?.shadowOffset = CGSize(width: 1.0, height: 1.0)
            vw?.shadowOpacity = 0.8
        }
        
        txtCompanyName.rightPaddingView = 55
        txtfirstName.placeholder = getCommonString(key: "First_Name_key")
        txtLastName.placeholder = getCommonString(key: "Last_Name_key")
        txtPhoneNumber.placeholder = getCommonString(key: "Phone_Number_key")
        txtCompanyName.placeholder = getCommonString(key: "Company/Organization_key")
        txtEmail.placeholder = getCommonString(key: "Email_key")
        txtPassword.placeholder = getCommonString(key: "Password_key")
        txtConfirmPassword.placeholder = getCommonString(key: "Confirm_Password_key")
        txtAuthenticationCode.placeholder = getCommonString(key: "Authentication_Code_key")
        
        btnSignUp.setThemeButtonUI()
        btnSignUp.setTitle(getCommonString(key: "Sign_Up_key"), for: .normal)
        
        self.theController?.addDoneButtonOnKeyboard(textfield: txtPhoneNumber)
        self.theController?.addDoneButtonOnKeyboard(textfield: txtAuthenticationCode)
        
    }
    
    @IBAction func btnSignUpTapped(_ sender: CustomButton)
    {
        if (txtfirstName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_first_name_key"))
        }
        else if (txtLastName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_last_name_key"))
        }
        else if (txtPhoneNumber.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_phone_number_key"))
        }
        else if (txtPhoneNumber.text?.isNumeric == false) || (txtPhoneNumber.text!.count < GlobalVariables.phoneNumberLimit)
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_valid_phone_number_key"))
        }
        else if (txtAuthenticationCode.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_authentication_code_key"))
        }
        else if (txtAuthenticationCode.text?.isNumeric == false)
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_valid_authentication_code_key"))
        }
            //        else if (txtCompanyName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
            //        {
            //            makeToast(strMessage: getCommonString(key:"Please_select_company/Organizatino_key"))
            //        }
        else if (txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_email_key"))
        }
            //        else if (self.batchListCheck == false)
            //        {
            //            makeToast(strMessage: getCommonString(key: "Please_enter_valid_email"))
            //        }
        else if !(self.theController!.isValidEmail(emailAddressString: txtEmail.text!))
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_valid_email"))
        }
        else if (txtPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_password_key"))
        }
        else if (txtPassword.text!.count) < 6
        {
            makeToast(strMessage: getCommonString(key:"Password_must_be_at_least_6_characters_long_key"))
        }
        else if (txtConfirmPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_confirm_password_key"))
        }
        else if(!(self.txtPassword.text == self.txtConfirmPassword.text))
        {
            makeToast(strMessage:getCommonString(key:"Password_and_confirmation_password_must_match_key"))
        }
        else
        {
            self.theController?.signUpModelView.signUpAPICalling()
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        if textField == txtPhoneNumber
        {
            let maxLength = GlobalVariables.phoneNumberLimit
            let currentString: NSString = txtPhoneNumber.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtCompanyName
        {
            self.theController?.view.endEditing(true)
            self.theController?.signUpModelView.companyNameDD.show()
            return false
        }
        
        return true
    }
    
    func configDD() {
        self.theController?.configDD(dropdown: (self.theController?.signUpModelView.companyNameDD)!, sender: self.txtCompanyName)
        self.theController?.signUpModelView.selectionIndex()
    }
}

