//
//  UserQuestionListVc.swift
//  Dracoinspect
//
//  Created by Haresh on 06/06/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import SwiftyJSON

class UserQuestionListVc: UIViewController {
    
    //MARK: - Variable
    
    lazy var theView: UserQuestionListView = { [unowned self] in
        return self.view as! UserQuestionListView
        }()
    
    lazy var theViewModel: UserQuestionListViewModel = {
        return UserQuestionListViewModel(theController: self)
    }()
    
    //MARK: - View life cycle
    
    var strJobName = ""
    var strJobId = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.theView.setupUI()

        NotificationCenter.default.addObserver(self, selector: #selector(reloadQuestionList), name: NSNotification.Name(rawValue: "reloadQuestionList"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: "\(getCommonString(key: "Task_key")): \(strJobName)")
    }
    
    @IBAction func btnFinishTapped(_ sender: UIButton) {
        
        if theViewModel.arrayTaskList!.contains(where: { (objTaskList) -> Bool in
            
            if objTaskList.isAnswerSubmitted == false
            {
                return true
            }
            return false
        })
        {
            makeToast(strMessage: getCommonString(key: "Please_submit_all_above_task_key"))
        }
        else
        {
            self.theViewModel.callFinishTaskAPI()
        }
        
    }
    
    func redirectToController(index: Int)
    {
        if let data = theViewModel.arrayTaskList?[index]
        {
            if data.qrCode == ""
            {
                //Redirect to answer screen
                
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "AnswerListVc") as! AnswerListVc
                obj.strTempletFormID = data.tblTemplateFormId!
                self.navigationController?.pushViewController(obj, animated: true)
 
            }
            else
            {
                //Redirect to QRCode Screen
                let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ScanQRCodeVC") as! ScanQRCodeVC
                obj.selectedController = .clickOnCellToCheckCodeUserSide
                obj.strTempletFormID = data.tblTemplateFormId!
                self.navigationController?.pushViewController(obj, animated: true)
            }
            
        }
        
    }
    
    
    @objc func reloadQuestionList(_ notification : NSNotification)
    {
        
        if let obj = notification.object as? JSON
        {
            
            if theViewModel.arrayTaskList!.contains(where: { (objTaskList) -> Bool in
                
                if let index = theViewModel.arrayTaskList!.index(where: { $0.tblTemplateFormId ==  obj["tbl_template_form_id"].stringValue})
                {
                    print("index : \(index)")
                    theViewModel.arrayTaskList?[index].isAnswerSubmitted = true
                    
                    self.theView.reloadQuestiontableData()
                    
                    return true
                    
                }
                
                return false
            })
            {
                theViewModel.arraySubmitFinalTask.append(obj)
                
                print("arrayFinalTaskList:\(theViewModel.arraySubmitFinalTask)")
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadMainJobListList"), object: nil)
                
            }
            else
            {
                
            }
            
        }
        
    }
    
    
}

//MARK: - TableView dataSource and delegate

extension UserQuestionListVc: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if theViewModel.arrayTaskList?.count == 0 || theViewModel.arrayTaskList == nil
        {
            let lbl = UILabel()
            lbl.text = "Question List is empty."
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appthemeRedColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        
        tableView.backgroundView = nil
        return theViewModel.arrayTaskList?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "QueWithQRCodeTblCell") as! QueWithQRCodeTblCell
        
        cell.setupData(data: (theViewModel.arrayTaskList?[indexPath.row])!, index: indexPath.row)
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let data = theViewModel.arrayTaskList?[indexPath.row]
        {
            if data.isAnswerSubmitted
            {
                makeToast(strMessage: "You already submit task")
                return
            }
            
            if indexPath.row != 0
            {
                if theViewModel.arrayTaskList?[indexPath.row - 1].isAnswerSubmitted == false
                {
                    makeToast(strMessage: getCommonString(key: "Please_submit_above_task_first_key"))
                }
                else
                {
                    redirectToController(index: indexPath.row)
                }
                
            }
            else
            {
               redirectToController(index: indexPath.row)
            }
            
        }
        
    }
    
}

