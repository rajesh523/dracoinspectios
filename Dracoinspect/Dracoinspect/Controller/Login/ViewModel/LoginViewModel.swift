//
//  LoginViewModel.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation
import FirebaseMessaging

class LoginViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:LoginVC!
    
    init(theController:LoginVC) {
        self.theController = theController
    }
    
    func loginAPICalling() {
        theController?.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(basicAdminURL)\(userURL)\(loginURL)"
            print("URL: \(url)")
            
            let param = ["email" : theController.loginView.txtEmail.text ?? "",
                         "password" : theController.loginView.txtPassword.text ?? "",
                         "device_token" : (forKey: "device_token") as? String ?? "",
                         "register_id" : Messaging.messaging().fcmToken ?? "",
                "device_type" : GlobalVariables.deviceType
            ]
            
            print("param :\(param)")
            theController?.showLoader()
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                self.theController?.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        let data = json["data"]
                        
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetails")
                        Defaults.synchronize()
                        print(getUserDetail("id"))
                        
                        appdelgate.templatelistAPICalling()
                        
                        if data["role"].stringValue == "1" {  //Admin
                            print("Admin")
                            
                            GlobalVariables.isUserLogin = false
                            
                            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "MainVC") as! MainVC
                            self.theController?.navigationController?.pushViewController(obj, animated: true)
                        }
                        else if data["role"].stringValue == "2" { // User
                            print("user")
                            
                            GlobalVariables.isUserLogin = true
                            
                            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ModifyJobSequenceVc") as! ModifyJobSequenceVc
                            self.theController?.navigationController?.pushViewController(obj, animated: true)
                            
                        }
                        
                        switch CLLocationManager.authorizationStatus()
                        {
                            case .notDetermined, .restricted, .denied:
                                appdelgate.openSetting()
                            case .authorizedAlways:
                                print("authorized")
                            case .authorizedWhenInUse:
                                print("authorized")
                        }
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.theController?.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
    
    func saveEmailPassword() {
        //save email/password
        if (theController.loginView.imgViewCheckMark.image == UIImage(named: "ic_check_icon_on")) {
            Defaults.set(theController.loginView.txtEmail.text, forKey: "emailStore")
            Defaults.set(theController.loginView.txtPassword.text, forKey: "passworldStore")
            Defaults.set("ic_check_icon_on", forKey: "imageCheckMark")
        }
        else if (theController.loginView.imgViewCheckMark.image == UIImage(named: "ic_check_icon_off")) {
            Defaults.removeObject(forKey: "emailStore")
            Defaults.removeObject(forKey: "passworldStore")
            Defaults.removeObject(forKey: "imageCheckMark")
        }
    }
}
