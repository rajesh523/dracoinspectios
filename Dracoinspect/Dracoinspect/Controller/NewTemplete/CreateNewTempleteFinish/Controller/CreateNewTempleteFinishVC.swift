//
//  CreateNewTempleteFinishVC.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 17/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class CreateNewTempleteFinishVC: UIViewController {
    
    
    //MARK:- Variables
    lazy var mainView: CreateNewTempleteFinishView = { [unowned self] in
        return self.view as! CreateNewTempleteFinishView
        }()
    
    lazy var mainViewModel: CreateNewTempleteFinishViewModel = {
        return CreateNewTempleteFinishViewModel(theController: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Create_a_new_task_key"))
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        mainView.setupUI(theDelegate: self)
    }    
}
