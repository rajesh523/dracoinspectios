//
//  CreateJobSequenceDetailsView.swift
//  Dracoinspect
//
//  Created by Haresh on 28/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class CreateJobSequenceDetailsView: UIView {

    //MARK: -  Outlet
    
    @IBOutlet weak var tblJobList: UITableView!
    @IBOutlet weak var btnDoneSelection: CustomButton!
    
    var upperReferesh = UIRefreshControl()

    
    func setupUI()
    {
        btnDoneSelection.setTitle(getCommonString(key: "Done_Selection_key"), for: .normal)
        btnDoneSelection.setThemeButtonUI()
        
        tblJobList.register(UINib(nibName: "selectJobTblCellTableViewCell", bundle: nil), forCellReuseIdentifier: "selectJobTblCellTableViewCell")
        tblJobList.tableFooterView = UIView()
        
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        
        tblJobList.addSubview(upperReferesh)
        
    }
    
    func tableReload()
    {
        self.tblJobList.reloadData()
    }
    
    @objc func upperRefreshTable()
    {
        (self.viewNextPresentingViewController() as? CreateJobSequenceDetailsVc)?.startUpperRefresh()
    }
    
    func stopUpperRefresh()
    {
        self.upperReferesh.endRefreshing()
    }
    
}

