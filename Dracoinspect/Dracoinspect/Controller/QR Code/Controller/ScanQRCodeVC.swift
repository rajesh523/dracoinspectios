//
//  ScanQRCodeVC.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import QRCodeReader
import AVFoundation
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON


enum checkParentScreenForOTP
{
    case clickOnCellToCheckCodeUserSide // Click on cell after enter api calling
    case submitAnswerUserSide // Enter code in answer 
    case fromNewTemplet
}


class ScanQRCodeVC: UIViewController {
    
    //MARK:- Variables
    lazy var mainView: ScanQRCodeView = { [unowned self] in
        return self.view as! ScanQRCodeView
        }()
    
    lazy var mainModelView: ScanQRCodeViewModel = {
        return ScanQRCodeViewModel(theController: self)
    }()
    
    
    var selectedController = checkParentScreenForOTP.fromNewTemplet
    
    var strTempletFormID = ""
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Draco_Inspect_key"))
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {        
        isScanScreenForIssue = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        mainView.stopScanning()
    }
    
    func setupUI() {
        mainView.setupUI(theDelegate: self)
    }
    
    
    func redirectToAnswerList()
    {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "AnswerListVc") as! AnswerListVc
        obj.strTempletFormID = self.strTempletFormID
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}
