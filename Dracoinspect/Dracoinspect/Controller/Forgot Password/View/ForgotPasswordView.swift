//
//  ForgotPasswordView.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class ForgotPasswordView: UIView, UITextFieldDelegate {

    //MARK:- Variables
    fileprivate weak var theController:ForgotPasswordVC!
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblForgotPassword: UILabel!
    @IBOutlet weak var lblForgotPasswordMsg: UILabel!    
    @IBOutlet weak var vwEmail: CustomView!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var btnSubmit: CustomButton!   

    func setupUI(theDelegate: ForgotPasswordVC)
    {
        self.theController = theDelegate
        lblForgotPassword.text = getCommonString(key: "Forgot_Password_key")
        lblForgotPassword.font = themeFont(size: 17, fontname: .regular)
        lblForgotPassword.textColor = UIColor.appThemeDarkGrayColor
        
        btnSubmit.setThemeButtonUI()
        btnSubmit.setTitle(getCommonString(key: "Submit_key"), for: .normal)
        
        txtEmail.delegate = self
        txtEmail.leftPaddingView = 55
        txtEmail.setThemeTextFieldUI()
        txtEmail.placeholder = getCommonString(key: "Email_key")
        
        vwEmail.cornerRadius = (vwEmail.bounds.height)/2
        vwEmail.borderWidth = 1.0
        vwEmail.borderColors = UIColor.appthemeRedColor
        vwEmail.shadowColors = UIColor.lightGray
        vwEmail.shadowRadius = 2.0
        vwEmail.shadowOffset = CGSize(width: 1.0, height: 1.0)
        vwEmail.shadowOpacity = 0.8
        
        lblForgotPasswordMsg.textColor = UIColor.lightGray
        lblForgotPasswordMsg.font = themeFont(size: 15, fontname: .regular)
        lblForgotPasswordMsg.text = getCommonString(key: "ForgotPassword_Msg_key")
        
    }
    
    @IBAction func btnSubmitTapped(_ sender:UIButton)
    {
        
        if (txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_email_key"))
        }
        else if !(self.theController.isValidEmail(emailAddressString: txtEmail.text!))
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_valid_email_key"))
        }
        else
        {
            self.theController.forgotPasswordModelView.forgotPasswordAPIcalling()
        }
        
    }
    
    @IBAction func btnCloseTapped(_ sender:UIButton)
    {
        self.theController.dismiss(animated: false, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}
