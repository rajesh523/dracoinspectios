//
//  UseAnExistingTempleteViewModel.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper

class UseAnExistingTempleteViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:UseAnExistingTempleteVC!
    
    var taskList: TemplateListModelClass?
    
    init(theController:UseAnExistingTempleteVC) {
        self.theController = theController
    }
    
    func templatelistAPICalling() {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(basicAdminURL)\(apiQuestionsURL)\(templateListURL)"
            print("URL: \(url)")
            let param = ["user_id" : getUserDetail("id")]
            print("param :\(param)")
            self.theController.showLoader()
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                self.theController.hideLoader()
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        let jsonModel = TemplateListModelClass(JSON: json.dictionaryObject!)
                        self.taskList = jsonModel
                        self.theController.mainView.reloadData()
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func removeTemplateAPICalling(formId:String, index:Int) {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(basicAdminURL)\(apiQuestionsURL)\(removeTemplateURL)"
            print("URL: \(url)")
            let param = ["user_id" : getUserDetail("id"), "tbl_template_form_id": formId]
            print("param :\(param)")
            self.theController.showLoader()
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                self.theController.hideLoader()
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        self.taskList?.data?.remove(at: index)
                        self.theController.mainView.reloadData()
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
}
