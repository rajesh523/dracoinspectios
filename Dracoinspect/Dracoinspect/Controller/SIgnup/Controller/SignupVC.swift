//
//  SignupVc.swift
//  Draco Support
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import DropDown
import SwiftyJSON

class SignupVC: UIViewController {
    
    //MARK:- Variables
    lazy var signUpView: SignupView = { [unowned self] in
        return self.view as! SignupView
        }()
    
    lazy var signUpModelView: SignupViewModel = {
        return SignupViewModel(theController: self)
    }()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        print("emailDomainNameString: ----  \(self.signUpModelView.arrayBatchListString)")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Draco_Support_key"))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {        
      signUpView.configDD()
    }
    
    //MARK:- Functions
    func setupUI() {
        signUpView.setupUI(theDelegate: self)
    }
}
