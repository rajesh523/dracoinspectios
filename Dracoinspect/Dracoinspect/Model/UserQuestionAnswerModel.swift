import Foundation 
import ObjectMapper 

class UserQuestionAnswerModel: Mappable { 

	var flag: NSNumber? 
	var msg: String? 
	var data: QuestionAnswerList?

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		flag <- map["flag"] 
		msg <- map["msg"] 
		data <- map["data"] 
	}
} 

class QuestionAnswerList: Mappable {

	var formDetail: [QAFormDetail]?

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		formDetail <- map["form_detail"] 
	}
} 

class QAFormDetail: Mappable {

	var tblTemplateFormId: String? 
	var updateFormId: String? 
	var userId: String? 
	var createdDate: String? 
	var modifiedDate: String? 
	var qrCode: String? 
	var questions: [QAQuestions]?
	var formName: String? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		tblTemplateFormId <- map["tbl_template_form_id"] 
		updateFormId <- map["update_form_id"] 
		userId <- map["user_id"] 
		createdDate <- map["created_date"] 
		modifiedDate <- map["modified_date"] 
		qrCode <- map["qr_code"] 
		questions <- map["questions"] 
		formName <- map["form_name"] 
	}
} 

class QAQuestions: Mappable {

	var status: String? 
	var tblTemplateFormId: String? 
	var createdDate: String? 
	var modifiedDate: String? 
	var optionList: [QAOptionList]?
	var questionTypeId: String? 
	var questionsName: String? 
	var questionsId: String?
    var dropdownValueStore : String?
    var enterValue : String?
    
	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		status <- map["status"] 
		tblTemplateFormId <- map["tbl_template_form_id"] 
		createdDate <- map["created_date"] 
		modifiedDate <- map["modified_date"] 
		optionList <- map["option_list"] 
		questionTypeId <- map["question_type_id"] 
		questionsName <- map["questions_name"] 
		questionsId <- map["questions_id"] 
	}
} 

class QAOptionList: Mappable {

	var questionsId: String? 
	var tblOptionsId: String? 
	var trueAnswer: String? 
	var createdDate: String? 
	var options: String?
    var isSelected  = "0"
    

	required init?(map: Map){
        
	} 

	func mapping(map: Map) {
		questionsId <- map["questions_id"] 
		tblOptionsId <- map["tbl_options_id"] 
		trueAnswer <- map["true_answer"] 
		createdDate <- map["created_date"] 
		options <- map["options"] 
	}
    
} 

