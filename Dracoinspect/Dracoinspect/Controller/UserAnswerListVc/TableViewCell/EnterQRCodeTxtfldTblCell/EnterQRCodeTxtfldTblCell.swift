//
//  EnterQRCodeTxtfldTblCell.swift
//  Dracoinspect
//
//  Created by Haresh on 07/06/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class EnterQRCodeTxtfldTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var txtEnterQRCode: CustomTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func settxtData(data : QAQuestions,index:Int)
    {
        self.txtEnterQRCode.text = data.enterValue
    }
    
}
