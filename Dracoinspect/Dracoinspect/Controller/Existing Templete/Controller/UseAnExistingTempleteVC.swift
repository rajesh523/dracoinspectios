//
//  UseAnExistingTempleteVC.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 20/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class UseAnExistingTempleteVC: UIViewController {

    //MARK:- Variables
    lazy var mainView: UseAnExistingTempleteView = { [unowned self] in
        return self.view as! UseAnExistingTempleteView
        }()
    
    lazy var mainModelView: UseAnExistingTempleteViewModel = {
        return UseAnExistingTempleteViewModel(theController: self)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Use_an_existing_task_key"))
        self.setupUI()
        // Do any additional setup after loading the view.
    }  
    
    func setupUI() {
        mainView.setupUI(theDelegate: self)
    }
}
