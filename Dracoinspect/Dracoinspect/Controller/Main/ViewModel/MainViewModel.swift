//
//  MainViewModel.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 22/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class MainViewModel {

    //MARK:- Variables
    fileprivate weak var theController:MainVC!
    
    init(theController:MainVC) {
        self.theController = theController
    }

}
