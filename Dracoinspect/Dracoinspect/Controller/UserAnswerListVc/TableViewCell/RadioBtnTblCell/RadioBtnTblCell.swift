//
//  RadioBtnTblCell.swift
//  Dracoinspect
//
//  Created by Haresh on 06/06/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class RadioBtnTblCell: UITableViewCell {

    //MARK: - Outlet
    @IBOutlet weak var lblAnswer: UILabel!
    @IBOutlet weak var btnSelectRadio: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setRadioData(data : QAOptionList,index:Int)
    {
        self.lblAnswer.text = data.options
        
        self.btnSelectRadio.isSelected = data.isSelected == "0" ? false : true
        
    }
    
}
