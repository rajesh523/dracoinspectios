//
//  CreateJobSequenceDetailsVc.swift
//  Dracoinspect
//
//  Created by Haresh on 28/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class CreateJobSequenceDetailsVc: UIViewController {

    lazy var theView: CreateJobSequenceDetailsView = { [unowned self] in
        return self.view as! CreateJobSequenceDetailsView
        }()
    
    lazy var theViewModel: CreateJobSequenceDetailsViewModel = {
        return CreateJobSequenceDetailsViewModel(theController: self)
    }()
    
    var strErrorMessage = ""
    
    var handlerForArray : ([AnyObject]) -> Void = {_ in}
    
    var selectedJob = ""
    
    var selectedController = checkCreateOrUpdateJob.create
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.theView.setupUI()
        
        theViewModel.callAddTaskList(isRefreshing: false) { (error) in
            print("error:\(error)")
            self.strErrorMessage = error
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if selectedController == .create
        {
            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Create_new_job_key"))
        }
        else if selectedController == .modify
        {
            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Modify_new_job_key"))
        }
        
    }
    
    @IBAction func btnDoneSelectionTapped(_ sender: UIButton) {
        
        theViewModel.selectedTaskList()
        
        guard let araryList = theViewModel.arrayTempletList else
        {
            return
        }
        
        if araryList.contains(where: { (templetList) -> Bool in
            
            if let dataList = templetList as? TempletList, dataList.selected
            {
                return true
            }
            
            return false
        })
        {
            print("some value selected")
            handlerForArray(theViewModel.selectedTempletList!)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
             makeToast(strMessage: getCommonString(key: "Please_select_from_above_key"))
        }
        
    }
    
    func startUpperRefresh()
    {
        
        self.theViewModel.callAddTaskList(isRefreshing: true) { (error) in
            print("error:\(error)")
        }
    }
    
}

extension CreateJobSequenceDetailsVc: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if theViewModel.arrayTempletList?.count == 0 || theViewModel.arrayTempletList == nil
        {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            
            return 0
        }
        
        tableView.backgroundView = nil
        return theViewModel.arrayTempletList?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectJobTblCellTableViewCell") as! selectJobTblCellTableViewCell
        
        cell.setupData(data: theViewModel.arrayTempletList![indexPath.row] as! TempletList)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        theViewModel.selectedCell(index: indexPath.row)
        
    }
    
    
}


