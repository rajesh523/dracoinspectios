//
//  CreateNewTempleteVC.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 17/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import SwiftyJSON

class CreateNewTempleteVC: UIViewController {
    
    //MARK:- Variables
    lazy var mainView: CreateNewTempleteView = { [unowned self] in
        return self.view as! CreateNewTempleteView
    }()
    
    lazy var mainViewModel: CreateNewTempleteViewModel = {
        return CreateNewTempleteViewModel(theController: self)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.mainViewModel.isEdit == true
        {
            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Modify_existing_task_key"))
        }
        else
        {
            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Create_a_new_task_key"))
        }
        
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        mainView.setupUI(theDelegate: self)
    }
}
