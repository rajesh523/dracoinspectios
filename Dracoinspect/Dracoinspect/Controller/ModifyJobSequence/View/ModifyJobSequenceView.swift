//
//  ModifyJobSequenceView.swift
//  Dracoinspect
//
//  Created by Haresh on 31/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class ModifyJobSequenceView: UIView {

    //MARK: - Outlet
    
    @IBOutlet weak var tblModifyJob: UITableView!
    
    var upperReferesh = UIRefreshControl()

    //MARK: - setupUI
    
    func setupUI()
    {
        tblModifyJob.register(UINib(nibName: "ModifyJobTblCell", bundle: nil), forCellReuseIdentifier: "ModifyJobTblCell")
        tblModifyJob.tableFooterView = UIView()
        
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        
        tblModifyJob.addSubview(upperReferesh)
    }
    
    func tableReload()
    {
        self.tblModifyJob.reloadData()
    }
    
    @objc func upperRefreshTable()
    {
        (self.viewNextPresentingViewController() as? ModifyJobSequenceVc)?.startUpperRefresh()
    }
    
    func stopUpperRefresh()
    {
        self.upperReferesh.endRefreshing()
    }
    
}
