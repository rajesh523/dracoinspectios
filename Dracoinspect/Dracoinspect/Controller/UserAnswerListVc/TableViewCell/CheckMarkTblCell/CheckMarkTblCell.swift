//
//  CheckMarkTblCell.swift
//  Dracoinspect
//
//  Created by Haresh on 06/06/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class CheckMarkTblCell: UITableViewCell {

    @IBOutlet weak var lblCheckMark: UILabel!
    @IBOutlet weak var btnCheckMark: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCheckMark(data : QAOptionList,index:Int)
    {
        self.lblCheckMark.text = data.options
        self.btnCheckMark.isSelected = data.isSelected == "0" ? false : true
        
    }
    
}
