//
//  selectJobTblCellTableViewCell.swift
//  Dracoinspect
//
//  Created by Haresh on 28/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit

class selectJobTblCellTableViewCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var btnCheckMark: UIButton!
    @IBOutlet weak var lblJob: UILabel!
    @IBOutlet weak var vwMain: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.vwMain.layer.borderColor = UIColor.black.cgColor
        self.vwMain.layer.borderWidth = 0.5
        self.vwMain.layer.cornerRadius = 4
        self.vwMain.layer.masksToBounds = true
        
        self.lblJob.font = themeFont(size: 15, fontname: .regular)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setupData(data:TempletList)
    {
        
        self.lblJob.text = data.formName
        
        if data.selected == false
        {
            self.btnCheckMark.isSelected = false
        }
        else
        {
            self.btnCheckMark.isSelected = true
        }
        
    }
    
    
}
