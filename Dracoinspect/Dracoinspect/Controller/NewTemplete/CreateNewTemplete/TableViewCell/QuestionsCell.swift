//
//  QuestionsCell.swift
//  Dracoinspect
//
//  Created by Haresh Bhai on 17/05/19.
//  Copyright © 2019 Haresh Bhai. All rights reserved.
//

import UIKit
import DropDown

protocol QuestionDelegate: class {
    func QuestionFinish(text: String, selectedType: Int, tag:Int)
}

class QuestionsCell: UITableViewCell, UITextFieldDelegate {

    weak var theController:CreateNewTempleteVC!

    @IBOutlet weak var lblQueNo: UILabel!
    @IBOutlet weak var txtQuestion: UITextField!
    
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var lblDropDownTitle: UILabel!
    
    @IBOutlet weak var btnClose: UIButton!
    let rightBarDropDown = DropDown()
    weak var delegate:QuestionDelegate?
    var selectedType:Int = 1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.lblQueNo.font = themeFont(size: 15, fontname: .regular)
        self.txtQuestion.font = themeFont(size: 15, fontname: .regular)
        self.lblDropDownTitle.font = themeFont(size: 15, fontname: .regular)

        self.txtQuestion.delegate = self
        rightBarDropDown.anchorView = btnSelect
        rightBarDropDown.dataSource = DropDownQuestion
        rightBarDropDown.backgroundColor = .white
        rightBarDropDown.width = btnSelect.bounds.width
        rightBarDropDown.bottomOffset = CGPoint(x: 0, y:btnSelect.bounds.height)
        
        rightBarDropDown.cellConfiguration = { (index, item) in return "\(item)" }
        
        rightBarDropDown.selectionAction = { (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.selectedType = DropDownQuestionTypeId[index]
            self.lblDropDownTitle.text = DropDownQuestion[index]
            self.lblDropDownTitle.textColor = .darkGray
            self.delegate?.QuestionFinish(text: self.txtQuestion.text!, selectedType: self.selectedType, tag: self.tag)
        }
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            self.delegate?.QuestionFinish(text: updatedText, selectedType: self.selectedType, tag: self.tag)
        }
        return true
    }
    
    func setDropDown(type:Int) {
        self.txtQuestion.delegate = self
        for (index, typeValue) in DropDownQuestionTypeId.enumerated() {
            if typeValue == type {
                self.selectedType = DropDownQuestionTypeId[index]
                self.lblDropDownTitle.text = DropDownQuestion[index]
            }
        }
        self.lblDropDownTitle.textColor = .darkGray
        self.delegate?.QuestionFinish(text: self.txtQuestion.text!, selectedType: self.selectedType, tag: self.tag)
    }

    func setupData(theDelegate: CreateNewTempleteVC, index:Int, data: QuestionModel) {
        self.theController = theDelegate
        self.tag = index
        self.btnClose.tag = index
        self.selectionStyle = .none
        self.lblQueNo.text = "Q\(index+1)."
        self.delegate = self.theController.mainViewModel
        self.txtQuestion.text = data.question
        
        self.setDropDown(type: data.type!)
    }
    
    
    func setupDataForEdit(theDelegate: CreateNewTempleteVC, index:Int, data: QuestionModel) {
        
        self.theController = theDelegate
        self.tag = index
        self.btnClose.tag = index
        self.selectionStyle = .none
        self.lblQueNo.text = "Q\(index+1)."
        self.delegate = self.theController.mainViewModel
        self.txtQuestion.text = data.question
        
        if data.questionId == "new"
        {
            self.btnSelect.isUserInteractionEnabled = true
        }
        else
        {
            self.btnSelect.isUserInteractionEnabled = false
        }
        
        self.setDropDown(type: data.type!)
    }
    
    
    
    
    @IBAction func btnSelectClicked(_ sender: Any) {
        rightBarDropDown.show()
    }
}
